﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestProtoAssetManager : MonoBehaviour {
    public AssetInformation asset;
    public UnityEngine.UI.Button bouton;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void TestTrigger(int i, int j, int k, int l)
    {
        if (i == asset.State.id)
        {
            Debug.Log("bon asset concerné");
            if (l == 0)
            {
                if (j==(int)AssetStatesInformation.assetTypeStates.NORMAL)
                {
                    UnityEngine.UI.ColorBlock cb = bouton.colors;
                    cb.normalColor = Color.red;
                    bouton.colors = cb;
                    asset.State.isAlert = true;
                    asset.State.state = AssetStatesInformation.assetTypeStates.ALERT;
                    return;
                }
                if(j==(int)AssetStatesInformation.assetTypeStates.ALERT)
                {
                    UnityEngine.UI.ColorBlock cb = bouton.colors;
                    cb.normalColor = Color.green;
                    bouton.colors = cb;
                    asset.State.isAlert = false;
                    asset.State.state = AssetStatesInformation.assetTypeStates.NORMAL;
                    return;
                }
            }
        }

    }

	// Update is called once per frame
	void Update () {
		
	}
}
