﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIProtoMover : MonoBehaviour {
    public RectTransform CoordUI;
    public RectTransform AnalystUI;
    public float leftMove;
    public float rightMove;
	// Use this for initialization
	void Start ()
    {
		
	}

    public IEnumerator LerpLeft(float f)
    {
        bool test = false;
        float timer = 0;
        while(!test)
        {
            timer += Time.deltaTime;
            CoordUI.anchoredPosition = Vector2.Lerp(CoordUI.anchoredPosition, new Vector2(-2005, 0), Time.deltaTime);
            AnalystUI.anchoredPosition = Vector2.Lerp(AnalystUI.anchoredPosition, new Vector2(0, 0), Time.deltaTime);
            if (timer > f)
            {
                CoordUI.anchoredPosition = new Vector2(-2005, 0);
                AnalystUI.anchoredPosition = new Vector2(0, 0);
                test = true;
                yield return null;
            }
            yield return null;
        }
        yield return null;
    }

    public IEnumerator LerpRight(float f)
    {
        bool test = false;
        float timer = 0;
        while (!test)
        {
            timer += Time.deltaTime;
            CoordUI.anchoredPosition = Vector2.Lerp(CoordUI.anchoredPosition, new Vector2(0, 0), Time.deltaTime);
            AnalystUI.anchoredPosition = Vector2.Lerp(AnalystUI.anchoredPosition, new Vector2(2005, 0), Time.deltaTime);
            if (timer > f)
            {
                CoordUI.anchoredPosition = new Vector2(0, 0);
                AnalystUI.anchoredPosition = new Vector2(2005, 0);
                test = true;
                yield return null;
            }
            yield return null;
        }
        yield return null;
    }

    public void MoveUILeft()
    {
        StartCoroutine(LerpLeft(1.5f));
    }
    public void MoveUIRight()
    {
        StartCoroutine(LerpRight(1.5f));
    }
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            MoveUILeft();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            MoveUIRight();
        }
    }
}
