﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(CyberCopEvent))]
public class CyberCopEventTriggerEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CyberCopEvent myScript = (CyberCopEvent)target;
        if (GUILayout.Button("Raise Event"))
        {
            myScript.Raise();
        }
    }

}

[CustomEditor(typeof(CyberCopEventUsers))]
public class CyberCopEventUsersTriggerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CyberCopEventUsers myScript = (CyberCopEventUsers)target;
        if (GUILayout.Button("Raise Event User"))
        {
            myScript.Raise();
        }
    }

}

[CustomEditor(typeof(CyberCopEventSystem))]
public class CyberCopEventSystemTriggerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CyberCopEventSystem myScript = (CyberCopEventSystem)target;
        if (GUILayout.Button("Raise Event System"))
        {
            myScript.Raise();
        }
    }

}

[CustomEditor(typeof(CyberCopEventAssets))]
public class CyberCopEventAssetTriggerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CyberCopEventAssets myScript = (CyberCopEventAssets)target;
        if (GUILayout.Button("Raise Event Asset"))
        {
            myScript.Raise();
        }
    }

}
[CustomEditor(typeof(CyberCopEventTickets))]
public class CyberCopEventTicketTriggerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CyberCopEventTickets myScript = (CyberCopEventTickets)target;
        if (GUILayout.Button("Raise Event Ticket"))
        {
            myScript.Raise();
        }
    }

}
