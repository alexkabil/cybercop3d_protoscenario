﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationButtonAssetEvents : MonoBehaviour
{
    private int _assetId=0;
    private int _alertType = 0;
    private int _state = 0;

    public AssetInfoList Scenario;
    public Transform Assets;
    public TestUIEvents InvestigationButtonOnAlertPressed;

    // Use this for initialization
	void Start ()
    {
		
	}
	
    public void InvestigationButton(int i, int j, int k, int l)
    {
        if (i != _assetId)
        {
            Debug.Log("mauvais asset d'investigation");//pour l'instant ici mais apres dans event receiver
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetInfoText(false);
            return;
        }
        else //on est sûr qu'il a été sélectionné..
        {
            if (/*Scenario.Assets[i - 1].State.isSelectedCoord && */Scenario.Assets[i-1].State.isAlert) //revérification, normalement en prenant en compte les parametre ijkl
            {
                //if (k == _alertType)
                //{
                    InvestigationButtonOnAlertPressed.Raise(i, j, k, l); //bon asset, bon texte
                    //return;
                //} 
            }
            else //ce cas peut arriver?
            {
                Debug.Log("cas bizarre de sélection de bouton d'investigation");
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetInfoText(false);
                return;

            }
        }

    }

    public void SetInformationAlert(int i, int j, int k, int l)
    {
        _assetId = i;
        _state = j;
        _alertType = k;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
