﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class IncidentButtonCoordEvent : MonoBehaviour
{
    public Button IncidentButton;
    public AssetInfoList Scenario;

    public TestUIEvents IncidentBothDeclared;

    private int _assetId=0;
    private int _state=0;
    private int _alertType = 0;
	// Use this for initialization
	void Start ()
    {
        IncidentButton.onClick.AddListener(IncidentButtonAction);
	}

    public void IncidentScenarioState(int i, int j, int k, int l)
    {
        _assetId = i;
        _state = j;
        _alertType = k;
    }
	
    public void IncidentButtonAction()
    {
        if (_assetId != 0)/*a voir comment traiter info, juste maj état?*/
        {

            if (Scenario.hasAssistance)
            {
                /*pas parfait mais simple*/
                IncidentButton.GetComponent<Image>().color = Color.white;
                IncidentButton.interactable = true;
            }
            Debug.Log("je lance le truc "+Scenario.GetCurrentAttack().GetCurrentAction());
            IncidentBothDeclared.Raise(_assetId, Scenario.currentAttack, 0, 0);

        /*
        if (Scenario.Assets[_assetId - 1].State.isIncidentAnalyst && Scenario.Assets[_assetId - 1].State.state == AssetStatesInformation.assetTypeStates.ALERT)
        {
            if (Scenario.hasAssistance)
            {
                IncidentButton.GetComponent<Image>().color = Color.white;
                IncidentButton.interactable = false;
            }

            IncidentBothDeclared.Raise(_assetId, 0, 0, 0);
            return;
        }*/

        }
        
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
