﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberPhysicalListenerActions : MonoBehaviour
{
    public VRTK_CyberCOP_Objects VRTK_ObjectSelecter;
    public VisualFilters VisualFilters;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void SetInteractionLayer(int i)
    {
        VRTK_ObjectSelecter.SetInteractionLayer(i);
    }

    public void CyberPhysicalView(int i)
    {
        VisualFilters.CyberPhysicalView(i);
        VisualFilters.AssetActivation(i);
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
