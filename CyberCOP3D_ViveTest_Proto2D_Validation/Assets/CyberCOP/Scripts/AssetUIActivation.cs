﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetUIActivation : MonoBehaviour
{
    public GameObject UIAsset;
    public AssetDataManager ADM;
    private int UIControl = 0;
    private bool activation = false;
    private Transform userHead;
    // Use this for initialization
	void Start ()
    {
        UIAsset.SetActive(false);

	}
	public void UiActivation(bool b)
    {
        UIAsset.SetActive(b);
    }

    public void UIActivation()
    {
        if (UIControl != 0)
        {
            return;
        }
        if (Camera.main.transform != null)
        {
            userHead = Camera.main.transform;
        }
        
        if(gameObject.name== "3DObject")
        {
            ADM.AssetSelection(true, 0);
        }
        
        activation = true;
        UIAsset.SetActive(true);
    }
    public void UIDeactivation()
    {
        if (UIControl != 0)
        {
            return;
        }
        activation = false;
        
        if (gameObject.name == "3DObject")
        {
            ADM.AssetSelection(false, 0);
        }
        
        UIAsset.SetActive(false);
    }
    public void UIStay()
    {
        if (UIControl == 0)
        {
            
            if (gameObject.name == "3DObject")
            {
                ADM.AssetClick(true,0);
            }
            else if (gameObject.name == "CyberObject")
            {
                ADM.AssetClick(true, 1);
            }

            UIControl =1;
            return;
        }
        if (UIControl == 1)
        {
            if (gameObject.name == "3DObject")
            {
                ADM.AssetClick(false,0);
            }
            else if (gameObject.name == "CyberObject")
            {
                ADM.AssetClick(false, 1);
            }
            UIControl = 0;
            return;
        }
        

    }
    // Update is called once per frame
    void Update ()
    {
        if (activation && userHead!=null)
        {
            UIAsset.transform.LookAt(2 * UIAsset.transform.position - userHead.position);
        }
	}
}
