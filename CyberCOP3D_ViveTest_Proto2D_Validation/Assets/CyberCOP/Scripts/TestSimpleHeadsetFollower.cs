﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSimpleHeadsetFollower : MonoBehaviour {
    public bool isOculus = true;
    public bool isVive = false;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(waitforOculus());
	}
	
    private IEnumerator waitforOculus()
    {
        yield return new WaitForSeconds(2.0f);
        if (isOculus)
        {
            if (GameObject.Find("RightEyeAnchor") != null)
            {
                transform.parent = GameObject.Find("RightEyeAnchor").transform;
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
            }
        }
        else if (isVive)
        {
            if (GameObject.Find("Camera (eye)") != null)
            {
                transform.parent = GameObject.Find("Camera (eye)").transform;
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
            }
        }
        yield return null;

    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
