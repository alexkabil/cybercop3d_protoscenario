﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CoordUIDisplay : MonoBehaviour {
    public GameObject UIDisplay;
    public GameObject AlertList;
    public UnityEngine.UI.Text DisplayText;
    public GameObject InvestigationButton;
    public AssetInfoList Scenario;
    public AnalystInvestigationButtonEvents InvestigationButtonAction;
    /*test de mise truc scénar ici plutot que sur le launcher*/
    //public RequiredActions ScenarioAction = RequiredActions.ALERTSELECTION;

    private int initstate = 0;
    private int _asset=0;
    private int _alert = 0;
    private int _alertType = 0;
    private int _alertUser = 0;
    private int _previousAlertButton = -1;
	// Use this for initialization
	void Start ()
    {
		
	}

    public void SetUICoordDisplay(int i, int j, int k, int l)
    {
        initstate++;
        if (initstate == 1)
        {
            if (UIDisplay != null)
            {
                UIDisplay.transform.GetChild(0).gameObject.SetActive(true);
                UIDisplay.transform.GetChild(1).gameObject.SetActive(true);
            }
        }
        if (l == Scenario.Currentuser.id)
        {
            if (k==(int)RequiredActions.ALERTSELECTION)
            {
                if (_previousAlertButton != -1 && AlertList.transform.childCount > 1)
                {
                    if (_alert != j)
                    {
                        var prevAlert = (from item in AlertList.GetComponentsInChildren<AlertButton>() where item.buttonID == _alert select item).First();
                        prevAlert.GetComponent<UnityEngine.UI.Image>().color = Color.grey;
                    }
                }
                var alert = (from item in AlertList.GetComponentsInChildren<AlertButton>() where item.buttonID == j select item).First();
                alert.GetComponent<UnityEngine.UI.Image>().color = Color.green;
                _previousAlertButton = _alert;
                _asset = i;
                _alert = j;
                _alertType = k;
                _alertUser = l;
                /*on fait la maj ici parce qu'il faut sélectionner alerte?*/
                //InvestigationButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
                InvestigationButtonAction.InitButtonState(i, j, (int)RequiredActions.ASSETINFORMATION, l); //mettre état a boutons

                DisplayText.text = "Alert " + j + " Asset " + i + " :\n";
                DisplayContextualText(Scenario.Attacks[j].cyberPhysical, (int)Scenario.Attacks[j].GetCurrentAction());
            }
            if (Scenario.currentAttack == j)
            {
                DisplayText.text = "Alert " + j + " Asset " + i + " :\n";
                DisplayContextualText(Scenario.Attacks[j].cyberPhysical, (int)Scenario.Attacks[j].GetCurrentAction());
            }
        }
        else
        {
            if (j == Scenario.currentAttack && initstate>=1)
            {
                //on met à jour en live les infos..
                DisplayText.text = "Alert " + j + " Asset " + i + " :\n";
                DisplayText.text += PreviousAction(j,l) +" "+ FutureAction(j,l);
            }
            else
            {
                //on met le bouton en surbrillance..
            }
        }
    }


    public void DisplayContextualText(int k, int l)
    {
        if (k == 0)
        {
            DisplayText.text += "High Entropy, ";
        }
        else if (k == 1)
        {
            DisplayText.text += "Network Issues, ";
        }
        switch (l)
        {
            case (int)RequiredActions.ASSETSELECTION:
                DisplayText.text += "Analyst must select asset";
                break;
            case (int)RequiredActions.ALERTINVESTIGATION:
                DisplayText.text += "Analyst must investigate";
                break;
            case (int)RequiredActions.ALERTSELECTION:
                DisplayText.text += "Alert must be selected";
                break;
            case (int)RequiredActions.ASSETINFORMATION:
                DisplayText.text += "Information should be taken";
                break;
            case (int)RequiredActions.ASSETINCIDENT:
                DisplayText.text += "Asset in in incident!";
                break;
            case (int)RequiredActions.INCIDENTVALIDATION:
                DisplayText.text += "Analyst must select asset";
                break;
            case (int)RequiredActions.NONE:
                DisplayText.text += "Analyst must select asset";
                break;
        }
    }

    public string PreviousAction(int i,int l)
    {
        var user = (from item in Scenario.Users where item.id == l select item).First();
        switch (i)
        {
            case (int)RequiredActions.ASSETSELECTION:
                DisplayText.text += user.Name+" a selectionne l'asset";
                break;
            case (int)RequiredActions.ALERTINVESTIGATION:
                DisplayText.text += user.Name + "a investigue";
                break;
            case (int)RequiredActions.ALERTSELECTION:
                DisplayText.text += user.Name + " a selectionne l'alerte";
                break;
            case (int)RequiredActions.ASSETINFORMATION:
                DisplayText.text += user.Name + " recolte de l'information";
                break;
            case (int)RequiredActions.ASSETINCIDENT:
                DisplayText.text += user.Name + " a qualifie l'incident";
                break;
            case (int)RequiredActions.INCIDENTVALIDATION:
                DisplayText.text += user.Name + " a valide l'incident";
                break;
            case (int)RequiredActions.NONE:
                DisplayText.text += user.Name + " a fini";
                break;
        }
        return "";
    }
    public string FutureAction(int i, int l)
    {
        var user = (from item in Scenario.Users where item.id == l select item).First();
        switch (i)
        {
            case (int)RequiredActions.ASSETSELECTION:
                DisplayText.text += user.Name + " doit selectionner l'asset";
                break;
            case (int)RequiredActions.ALERTINVESTIGATION:
                DisplayText.text += user.Name + " doit investiguer";
                break;
            case (int)RequiredActions.ALERTSELECTION:
                DisplayText.text += user.Name + " doit selectionner l'alerte";
                break;
            case (int)RequiredActions.ASSETINFORMATION:
                DisplayText.text += user.Name + " doit prendre des infos";
                break;
            case (int)RequiredActions.ASSETINCIDENT:
                DisplayText.text += user.Name + " doit caracteriser incident";
                break;
            case (int)RequiredActions.INCIDENTVALIDATION:
                DisplayText.text += user.Name + " doit valider incident";
                break;
            case (int)RequiredActions.NONE:
                DisplayText.text += user.Name + " doit rien faire";
                break;
        }
        return "";
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
