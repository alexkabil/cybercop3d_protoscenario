﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using UnityEngine.UI;
public class AssetDataManager : MonoBehaviour {

    public AssetInformation AssetInfo;
    public AlertCoordLauncher AlertCoord;
    private bool isGeneratingEntropy = true;
    public GraphChart graph;

    public AssetsSelection AssetSelected;
    public InformationButtonAssetEvents InformationButtonEvent;
    public IncidentButtonAssetEvents IncidentButtonEvent;

    private Transform PhysicalUI;
    private Transform CyberUI;
    private Button PhysInformationButton;
    private Button PhysIncidentButton;
    private Button CyberInformationButton;
    private Button CyberIncidentButton;

    private ParticleSystem PhysParticles;
    private ParticleSystem CyberParticles;

    private Text AssetText;
    private Text CyberText;

    private bool isAssetSelected = false;

    private float lowpart = -10.0f;
    private float highpart = 10.0f;
    // Use this for initialization
	void Start ()
    {
        DrawGraph();
        StartCoroutine(EntropyValue());

        PhysicalUI = transform.GetChild(0).GetChild(0).GetChild(1);
        CyberUI = transform.GetChild(1).GetChild(0).GetChild(0);

        PhysInformationButton = PhysicalUI.GetChild(2).GetComponent<Button>();
        PhysInformationButton.onClick.AddListener(GetAssetPhysInfo);

        PhysIncidentButton = PhysicalUI.GetChild(3).GetComponent<Button>();
        PhysIncidentButton.onClick.AddListener(GetPhysIncidentButton);

        CyberInformationButton = CyberUI.GetChild(2).GetComponent<Button>();
        CyberInformationButton.onClick.AddListener(GetAssetCyberInfo);

        CyberIncidentButton = CyberUI.GetChild(3).GetComponent<Button>();
        CyberIncidentButton.onClick.AddListener(GetCyberIncidentButton);

        AssetText = PhysicalUI.GetChild(1).GetComponent<Text>();
        CyberText = CyberUI.GetChild(1).GetComponent<Text>();

        PhysicalUI.GetChild(1).gameObject.GetComponent<Text>().text = AssetInfo.InitPhysicInfo();
        CyberUI.GetChild(1).gameObject.GetComponent<Text>().text = AssetInfo.InitCyberInfo();

        PhysParticles = transform.GetChild(2).GetComponent<ParticleSystem>();
        CyberParticles = transform.GetChild(3).GetComponent<ParticleSystem>();

    }
    private void OnDisable()
    {
        isGeneratingEntropy = false;
    }

    public void SetInfoText(bool b)
    {
        if (b)
        {
            AssetText.text = AssetInfo.AllPhysInfo();
        }
        else
        {
            AssetText.text = AssetInfo.PhysInfo();
        }
    }

    public void SetCyberInfoText(bool b)
    {
        if (b)
        {
            CyberText.text = AssetInfo.AllCyberInfo();
        }
        else
        {
            CyberText.text = AssetInfo.CyberInfo();
        }
    }

    public void GetIncidentButtonText(string s)
    {
        PhysIncidentButton.transform.GetChild(0).GetComponent<Text>().text = s;
    }

    public void GetPhysIncidentButton()
    {
        IncidentButtonEvent.AnalystDeclareIncidentAlertAsset(AssetInfo.State.id,0,0,0); 
    }

    public void GetAssetPhysInfo()
    {
        InformationButtonEvent.InvestigationButton(AssetInfo.State.id, 0, 0, 0);//changer ça pour envoyer etat?
    }

    public void GetAssetCyberInfo()
    {
        InformationButtonEvent.InvestigationButton(AssetInfo.State.id, 0, 1, 0);//changer ça pour envoyer etat?
        //CyberText.text = AssetInfo.CyberInfo();
    }

    public void GetCyberIncidentButton()
    {
        IncidentButtonEvent.AnalystDeclareIncidentAlertAsset(AssetInfo.State.id, 0, 1, 0);
    }
    public void GetCyberIncidentButtonText(string s)
    {
        CyberIncidentButton.transform.GetChild(0).GetComponent<Text>().text = s;
    }
    public IEnumerator EntropyValue()
    {
        while (isGeneratingEntropy)
        {
            AssetInfo.entropy.SetValue(Random.Range(lowpart,highpart));

            if (isAssetSelected)
            {
                graph.DataSource.StartBatch();
                graph.DataSource.AddPointToCategoryRealtime("Entropy", (double)AssetInfo.entropy.index, (double)AssetInfo.entropy.Value);
                graph.DataSource.EndBatch();
            }
            yield return new WaitForSeconds(1.0f);
        }
        yield return null;
    }

    public void DrawGraph()
    {
        if (graph != null)
        {
            graph.DataSource.StartBatch();
            graph.DataSource.ClearCategory("Entropy");
            if (AssetInfo.entropy.prevValues != null)
            {
                foreach (var pair in AssetInfo.entropy.prevValues)
                {
                    graph.DataSource.AddPointToCategory("Entropy", (double)pair.Key, (double)pair.Value);
                }
            }
            graph.DataSource.EndBatch();
        }
    }

    public void SetAlert(int i, int j, int k, int l)
    {
        AssetInfo.State.isAlert = true;
        AssetInfo.State.state = AssetStatesInformation.assetTypeStates.ALERT;
        /*L'envoi d'event de creation du bouton d'alerte est lié à l'evenemetn d'atk*/
        //AlertCoord.AlertEvent(i, j, k, l);
    }
    /*En dur ici faux positifs*/
    public void SetEntropyProcesses()
    {
        if (AssetInfo.State.id == 3)
        {
            AssetInfo.processes.Add("WannaCrypt0r.exe");
            AssetInfo.processes.Add("C2Command.exe");
        }
    }

    public void SetHighEntropy()
    {
        lowpart = 40.0f;
        highpart = 100.0f;
    }

    public void AssetSelection(bool b, int i)
    {
        if (i == 0)
        {
            isAssetSelected = b;
            if (isAssetSelected)
            {
                DrawGraph();
            }
        }
    }

    public void HighlightPhysInfoButton(bool b)
    {
        if (b)
        {
            PhysInformationButton.GetComponent<Image>().color = new Color(0.2f, 0.9f, 0.3f, 0.8f);
        }
        else
        {
            PhysInformationButton.GetComponent<Image>().color = Color.white;
        }
    }
    public void HighlightPhysIncidentButton(bool b)
    {
        if (b)
        {
            PhysIncidentButton.GetComponent<Image>().color = new Color(0.2f, 0.9f, 0.3f, 0.8f);
        }
        else
        {
            PhysIncidentButton.GetComponent<Image>().color = Color.white;
        }
    }

    public void HighlightCyberInfoButton(bool b)
    {
        if (b)
        {
            CyberInformationButton.GetComponent<Image>().color = new Color(0.8f, 0, 0.8f, 0.8f);
        }
        else
        {
            CyberInformationButton.GetComponent<Image>().color = Color.white;
        }
    }
    public void HighlightCyberIncidentButton(bool b)
    {
        if (b)
        {
            CyberIncidentButton.GetComponent<Image>().color = new Color(0.8f, 0, 0.8f, 0.8f);
        }
        else
        {
            CyberIncidentButton.GetComponent<Image>().color = Color.white;
        }
    }

    public void SetInteractableStateIncidentButton(bool b)
    {
        PhysIncidentButton.interactable = b;
    }
    public void SetInteractableStateCyberIncidentButton(bool b)
    {
        CyberIncidentButton.interactable = b;
    }

    public void HighlightObject(bool b, int i)
    {
        if (i == 0) //physical world
        {
            if (b)
            {
                PhysParticles.Play();
            }
            else
            {
                PhysParticles.Stop();
            }
            return;
        }
        if (i == 1) //cyber world
        {
            if (b)
            {
                CyberParticles.Play();
            }
            else
            {
                CyberParticles.Stop();
            }
            return;
        }
    }

    public void AssetClick(bool b, int i)
    {
        //AssetInfo.State.isSelectedAnalyst = b;

        if (b)
        {
            AssetSelected.AssetSelected(AssetInfo.State.id, 0, i, 0);
        }
        
    }


    // Update is called once per frame
    void Update () {
		
	}
}
