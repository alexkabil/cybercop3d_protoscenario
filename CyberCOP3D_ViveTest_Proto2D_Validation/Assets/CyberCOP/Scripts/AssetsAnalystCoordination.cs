﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsAnalystCoordination : MonoBehaviour
{
    public AssetInfoList Scenario;
    public Transform Assets;

    public AnalystInvestigationButtonEvents InvestigationButtonEvents;

    public AssetsSelection AssetsSelection;

    public Transform AssetOnMap;

    public RequiredActions ScenarioAction = RequiredActions.ALERTINVESTIGATION;
	// Use this for initialization
	void Start ()
    {
		
	}

    public void SetNextStepScenario()
    {
        if (Scenario.GetCurrentAttack().GetCurrentAction() == ScenarioAction)
        {
            Scenario.GetCurrentAttack().NextScenarioAction();
        }
    }

    public void SetInvestigationOnMap(int i, int j, int k, int l)
    {
        AssetOnMap.GetChild(i - 1).GetChild(0).GetComponent<UnityEngine.UI.Image>().enabled = true;
    }
    /*k c'est l'action passée !*/
    public void AnalystCoordAlert(int i, int j, int k, int l)
    {
        if (Scenario.hasAssistance)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightObject(true, Scenario.GetAttack(j).cyberPhysical);
        }
        //Scenario.Assets[i - 1].State.isSelectedCoord = true;
    }
    
    public void BlockInvestigationButton(bool b)
    {
        InvestigationButtonEvents.BlockInvestigationButton(b);
    }

    public void SetInvestigationSelectionNeeds(int i, int j, int k, int l)
    {
        AssetsSelection.SetInvestigationNeeds(i, j, k, l);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
