﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsEntropyEventManager : MonoBehaviour
{
    public Transform Assets;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void ChangeGraphValue(int i, int j, int k, int l)
    {
        Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetHighEntropy();
    }
    /*Fonction unitaire alerte?*/
    public void AssetChangeState(int i, int j, int k, int l)
    {
        Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetAlert(i,j,k,l);
    }
    public void AssetAddMaliciousProcesses(int i, int j, int k, int l)
    {
        Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetEntropyProcesses();
    }
    // Update is called once per frame
    void Update ()
    {
		
	}
}
