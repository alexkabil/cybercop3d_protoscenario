﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalystInformationOnAlert : MonoBehaviour
{
    public AssetInfoList Scenario;
    public Transform Assets;
    public Transform AssetsOnMap;

    public IncidentButtonAssetEvents IncidentButtonAssetEvents;
    public RequiredActions ScenarioAction = RequiredActions.ASSETINFORMATION;

	// Use this for initialization
	void Start ()
    {
		
	}
    public void SetNextStepScenario()
    {
        if (Scenario.GetCurrentAttack().GetCurrentAction() == ScenarioAction)
        {
            Scenario.GetCurrentAttack().NextScenarioAction();
        }
    }
    public void SetInformationTextAsset(int i, int j, int k, int l)
    {
        if (k == 0)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetInfoText(true);
            if (Scenario.hasAssistance)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightPhysInfoButton(false);
            }
        }
        if (k == 1)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().SetCyberInfoText(true);
            if (Scenario.hasAssistance)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightCyberInfoButton(false);
            }
        }
    }

    public void SetAssetOnMapInvestigation(int i, int j, int k, int l)
    {
        AssetsOnMap.GetChild(i - 1).GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.yellow;
    }

    public void ScenarioIncidentDeclarationAnalyst(int i, int j, int k, int l)
    {
        if (k == 0)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().GetIncidentButtonText("Declare Incident");
            if (Scenario.hasAssistance)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightPhysIncidentButton(true);
            }
        }
        if (k == 1)
        {
            Assets.GetChild(i - 1).GetComponent<AssetDataManager>().GetCyberIncidentButtonText("Declare Net Incident");
            if (Scenario.hasAssistance)
            {
                Assets.GetChild(i - 1).GetComponent<AssetDataManager>().HighlightCyberIncidentButton(true);
            }
        }
    }

    public void SetIncidentScenarioInformation(int i, int j, int k, int l)
    {
        IncidentButtonAssetEvents.SetInformationIncident(i,j,k,l);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
