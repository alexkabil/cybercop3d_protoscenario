﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AssetIncidentConfirmation : MonoBehaviour 
{
    public AssetInfoList Scenario;

    public GameObject IncidentPrefab;
    public Transform IncidentButtonParent;
    public Transform AlertButtonParent;
    public Transform AssetsOnMap;

    public UnityEngine.UI.Text CoordUIText;
    public RequiredActions ScenarioAction = RequiredActions.INCIDENTVALIDATION;

	// Use this for initialization
	void Start ()
    {
		
	}
    public void SuppressAlert(int i, int j, int k, int l)
    {
        //if (AlertButtonParent.childCount > Scenario.currentAttack)
        //{
        var alert = (from item in AlertButtonParent.GetComponentsInChildren<AlertButton>() where item.buttonID == Scenario.currentAttack select item).First();
        Destroy(alert.gameObject);
        //Destroy(AlertButtonParent.GetChild((Scenario.alertNumber-1)- Scenario.currentAttack).gameObject);
        Scenario.alertNumber--;
        //}
    }

    public void CoordUIIncidentText()

    {
        CoordUIText.text = "L'incident a été déclaré par plusieurs sources. L'alerte est donc escaladée.";
    }

    public void CreateIncident(int i, int j, int k, int l)
    {
        Debug.Log(Scenario.GetCurrentAttack().GetCurrentAction() + "  " + Scenario.GetCurrentAttack().attackState);
        if (Scenario.GetCurrentAttack().GetCurrentAction() == ScenarioAction)
        {
            Scenario.incidentNumber++;
            var button = GameObject.Instantiate(IncidentPrefab, IncidentButtonParent);
            button.GetComponent<IncidentButton>().SetButtonState(i, j);

            button.transform.SetAsFirstSibling();

            IncidentButtonParent.GetComponent<RectTransform>().sizeDelta += new Vector2(0, button.GetComponent<RectTransform>().sizeDelta.y);
            Scenario.GetCurrentAttack().NextScenarioAction();
            CoordUIIncidentText();
            SuppressAlert(i,j,k,l);
        }
        
    }

	public void SetIncidentInfoOnMap(int i, int j, int k, int l)
    {
        AssetsOnMap.GetChild(i - 1).GetComponent<UnityEngine.UI.Image>().color = new Color(0.2f, 0.2f, 0.2f, 0.8f);
    }
	
    
    // Update is called once per frame
	void Update ()
    {
		
	}
}
