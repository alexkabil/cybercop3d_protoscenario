﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CoordUIReset : MonoBehaviour
{
    public Text CoordUIText;
    public GameObject InvestigationButton;
    public GameObject IncidentButton;
    public AssetInfoList Scenario;
    private int _alertbuttonID = 0;
    private int _previousAlertbuttonID = -2;
    private int _alertUIState = 0;
	// Use this for initialization
	void Start ()
    {
		
	}

    public void ResetCoordUI(int i, int j, int k, int l)
    {
        //pour l'instant pas de prise en compte de l'état précédent..
        CoordUIText.text = ".............................";
        InvestigationButton.GetComponent<Image>().color = Color.white;
        InvestigationButton.GetComponent<Button>().interactable = true;
        IncidentButton.GetComponent<Image>().color = Color.white;
        IncidentButton.GetComponent<Button>().interactable = true;
        if (Scenario.hasAssistance)
        {
            InvestigationButton.transform.GetChild(1).GetComponent<ParticleSystem>().Stop();
        }
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
