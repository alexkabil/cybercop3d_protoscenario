﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//boutons quand on sélectionne une alerte sur la vue coord.
public class AlertActionsButtons : MonoBehaviour, IPointerClickHandler {

    public RequiredActions ButtonAction;
    private CyberCopEventsInformation contextualEvent;
    private bool _isActive = true;

    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;

    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void SetButtonState(CyberCopEventsInformation e)
    {
        contextualEvent = e;

        GetComponent<UnityEngine.UI.Button>().interactable = (int)e.Action == (int)ButtonAction ? true : false; //faire du highlight plutot que de l'activation?
        GetComponent<UnityEngine.UI.Image>().color = Color.white;
    }
    public void ActionAlreadyDone()
    {
        GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f,0.4f,0.8f,0.5f);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        //GetComponent<UnityEngine.UI.Image>().color = Color.blue;
        _isActive = !_isActive;//garder en mémoire que l'action a été faite?? faire le lien avec la liste des actions??
        CyberCopEventsInformation click = new CyberCopEventsInformation(contextualEvent.Asset, contextualEvent.View, (int)ButtonAction, contextualEvent.User, contextualEvent.TicketID);

        ButtonDelegate(click); //on ne peut appuyer que si c'est la nextaction? ou alors on passe toujours le buttonAction?
        //throw new System.NotImplementedException();
    }
}
