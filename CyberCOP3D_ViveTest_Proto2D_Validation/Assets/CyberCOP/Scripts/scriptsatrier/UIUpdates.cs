﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIUpdates : MonoBehaviour {
	public AssetInfoList Scenario;
	
	public Transform UI2D;
	public Transform UI3D;

	private int currentAsset=-1;
	private int currentTicket = -1;

    public Text AlertText;
    public Transform AlertUIButtonsParent;

	private void OnEnable() 
	{
        AlertUpdateEventListener.AlertUIUpdateDelegate += AlertUIModification;
        TicketUpdateEventListener.AlertUIUpdateDelegate += AlertUIModification; //test sur la progression du ticket pour l'alerte...

		AssetsUpdateEventListener.UIUpdateDelegate+=AssetUIModification;
	}
	private void OnDisable() 
	{
        AlertUpdateEventListener.AlertUIUpdateDelegate -= AlertUIModification;
        AssetsUpdateEventListener.UIUpdateDelegate-=AssetUIModification;
        TicketUpdateEventListener.AlertUIUpdateDelegate -= AlertUIModification; //test sur la progression du ticket pour l'alerte...
    }
	
	// Use this for initialization
	void Start () 
	{
		foreach(Transform tr in AlertUIButtonsParent)
        {
            tr.gameObject.SetActive(false);
        }
	}
	
	public void AlertUIModification(CyberCopEventsInformation e)
	{
        if (!Scenario.Currentuser.isImmersive)
        {
            //faire un filtre sur e.user?
            if (e.TicketID == Scenario.Currentuser.CurrentTicketID) //un utilisateur a selectionné ce ticket et ui visible du coup par le current user
            {
                var attack = Scenario.GetAttack(e.TicketID);
                if(e.User == Scenario.Currentuser.id) //si c'est moi qui modifie des trucs
                {
                    //c'est moi qui sélectionne?
                    //Nextaction à fournir aux boutons de l'ui..
                    CyberCopEventsInformation contextualAction = new CyberCopEventsInformation(attack.assetConcerned, attack.cyberPhysical, (int)attack.GetCurrentAction(), e.User, e.TicketID);
                    //appel à l'ui 2D
                    foreach (Transform tr in AlertUIButtonsParent)
                    {
                        tr.gameObject.SetActive(true);
                        if (tr.gameObject.GetComponent<AlertActionsButtons>() != null)
                        {
                            AlertActionsButtons alertbutton = tr.gameObject.GetComponent<AlertActionsButtons>();
                            alertbutton.SetButtonState(contextualAction);
                            if (attack.ActionsPerformed.Contains(alertbutton.ButtonAction))
                            {
                                alertbutton.ActionAlreadyDone();
                            }
                        }
                        //Appel direct de la fonction, utiliser sendmessage à la place??
                    }
                    string currentAction = "Next action: " + System.Enum.GetName(typeof(RequiredActions), attack.GetCurrentAction()) + " \n";
                    string alertState = "Alert ID : " + e.TicketID + "   user: " + e.User +" Asset: "+e.Asset+ "  \n";
                    foreach (RequiredActions re in attack.ActionsPerformed)
                    {
                        alertState += " done :" + System.Enum.GetName(typeof(RequiredActions), re) + "\n";
                    }
                    AlertText.text = currentAction + alertState;
                }

            }
            if (Scenario.Currentuser.CurrentTicketID == -1) //message de deactivation de l'alerte?
            {
                AlertText.text = "";
                //deactivation des boutons?
                foreach(Transform tr in AlertUIButtonsParent)
                {
                    tr.gameObject.SetActive(false);
                }
            }
        }
	}



	public void AssetUIModification(CyberCopEventsInformation e)
	{
		//if(e.Action == (int)AssetAct)
		Debug.Log("maj ui pour les assets");
		if(!Scenario.Currentuser.isImmersive)
		{

		}
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
}
