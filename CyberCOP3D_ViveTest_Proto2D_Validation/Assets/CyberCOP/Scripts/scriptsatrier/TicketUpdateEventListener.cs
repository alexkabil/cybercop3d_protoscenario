﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class TicketTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;

    public TicketTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner;
        this.type = type;
    }

    public TicketTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
    }
}

public class TicketUpdateEventListener : MonoBehaviour {
    public AssetInfoList Scenario;

    private CyberCopEventsInformation previousTicketUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);

    private TicketTest currentTicket = new TicketTest();
    private List<TicketTest> TicketList = new List<TicketTest>();

    public delegate void OnAlertUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnAlertUIUpdate AlertUIUpdateDelegate;

    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void TicketUpdateState(CyberCopEventsInformation e, bool b)
    {
        //mettre une garde sur le doublon de tickets?
        /*
        if (previousTicketUpdate.Action == e.Action && previousTicketUpdate.Asset == e.Asset && previousTicketUpdate.TicketID == e.TicketID && previousTicketUpdate.User == e.User && previousTicketUpdate.View == e.View)
        {
            Debug.Log("meme event ticket");
            return;
        }
        */
        previousTicketUpdate = e;

        switch (e.Action)
        {
            case (int)TicketUpdateActions.TICKETCREATION:
                TicketCreation(e,1);
                break;
            case (int)TicketUpdateActions.TICKETSELECTION:
                TicketSelection(e);
                break;
            case (int)TicketUpdateActions.TICKETPROGRESSION:
                TicketProgression(e);
                break;
            default:
                break;
        }
    }

    public void TicketCreation(CyberCopEventsInformation e, int type)
    {
        TicketList.Add(new TicketTest(e.TicketID,0,e.User,type,false)); //création de ticket, de type 1. Quand escalation, changement type.
        Debug.Log("objet ticket   cree");
    }

    public void TicketSelection(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var ticket = (from element in TicketList where element.id == e.TicketID select element).FirstOrDefault();
        if(user.CurrentTicketID!=-1 && user.CurrentTicketID != e.TicketID) //non, ce qui compte c'est la previously selected by user.
        {
            var prevticket = (from element in TicketList where element.id == user.CurrentTicketID select element).FirstOrDefault();
            prevticket.isSelected = false;
            Debug.Log("selection autre "+prevticket.id+" "+ticket.id);
        }
        if (ticket != null)
        {
            currentTicket = ticket;

            user.CurrentTicketID = e.TicketID; //changement ticket user, currentuser or users[e.user]??
            ticket.isSelected = true;
            Debug.Log("ticket selectionne ");

            //en fonction des actions nécessaires, on affiche des trucs?
        }
        else
        {
            Debug.Log("le ticket n'existe pas!");
        }
    }

    public void TicketProgression(CyberCopEventsInformation e)
    {
        Scenario.GetAttack(e.TicketID).NextScenarioAction();
        currentTicket.progress++;
        Debug.Log("Ticket Progression");
        AlertUIUpdateDelegate(e);
        switch (Scenario.GetAttack(e.TicketID).GetCurrentAction())
        {
            case RequiredActions.ASSETSELECTION_ANALYST:
                break;
            case RequiredActions.ASSETUNSELECTION_ANALYST:
                break;
            default:
                break;
        }
        if (Scenario.GetAttack(e.TicketID).GetCurrentAction() == RequiredActions.NONE)
        {
            //on est à la fin du ticket, on change le scenar par un event ou un delegate..
            Debug.Log("ticket fini");
        }
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
