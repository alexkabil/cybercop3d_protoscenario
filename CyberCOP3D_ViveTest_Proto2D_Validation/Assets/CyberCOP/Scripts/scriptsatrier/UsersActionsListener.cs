﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UsersActionsListener : MonoBehaviour {

    public AssetInfoList Scenario;

    public delegate void OnAssetStateUpdate(CyberCopEventsInformation e);
    public static event OnAssetStateUpdate AssetUpdateDelegate;

    public delegate void OnTicketStateUpdate(CyberCopEventsInformation e);
    public static event OnTicketStateUpdate TicketUpdateDelegate;
    public delegate void OnAlertStateUpdate(CyberCopEventsInformation e);
    public static event OnAlertStateUpdate AlertUpdateDelegate;

    private CyberCopEventsInformation previousUserAction = new CyberCopEventsInformation(999,999,999,999,999);

    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void UsersActionsReceived(CyberCopEventsInformation e, bool b)
    {
        if (previousUserAction.Action ==e.Action && previousUserAction.Asset==e.Asset && previousUserAction.TicketID == e.TicketID && previousUserAction.User == e.User && previousUserAction.View == e.View)
        {
            Debug.Log("meme event user");
            return;
        }
        previousUserAction = e;
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault(); 

        //filtrage par utilisateur d'abord? 0==me filtrage en fonction du scénario??
        if (e.User == Scenario.Currentuser.id)//utilisateur 
        {
                //pour filtrer en fonction du role de l'user ??
                /*
                var item = (from it in Scenario.Users where it.id == e.User select it).FirstOrDefault();
                if(item.userRole == Roles.ANALYST)
                {

                }
                */
            switch (e.Action)
            {

                case (int)RequiredActions.ASK_ANALYST_KINETICINVESTIGATION: //on transmet aussi l'action necessaire?
                    TicketUpdateDelegate(new CyberCopEventsInformation((int)RequiredActions.ASSETINFORMATION_ANALYST, e.View, (int)TicketUpdateActions.TICKETCREATION, e.User, e.TicketID));//soit on transmet ask soit on transmet direct l'action à faire..
                    break;
                case (int)RequiredActions.ALERTSELECTION_ANALYST: //ici faut transmettre le ticket sur un bouton ou autre..
                    TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ALERTSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ALERTUNSELECTION: //bug
                    AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTUNSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ASSETSELECTION_ANALYST:// on laisse ca comme ca pour l'instant
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION_ANALYST:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETSELECTION:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, user.id, user.CurrentTicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, user.id, user.CurrentTicketID));
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (e.Action)
            {
                case (int)RequiredActions.ASSETSELECTION:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETSELECTION, e.User, e.TicketID));
                    break;
                case (int)RequiredActions.ASSETUNSELECTION:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ASSETUNSELECTION, e.User, e.TicketID));
                    break;
                default:
                    break;
            }
        }
        //faire correspondre le ticket et la position des attaques dans le scenario
        if (user.CurrentTicketID != -1 && (int)Scenario.GetAttack(user.CurrentTicketID).GetCurrentAction() == e.Action && (int)Scenario.GetAttack(user.CurrentTicketID).assetConcerned==e.Asset && (int)Scenario.GetAttack(user.CurrentTicketID).cyberPhysical==e.View)
        {
            //on perd l'info de l'action, mais c'est pas grave? ticket progression séparé d'alerte progression??
            TicketUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)TicketUpdateActions.TICKETPROGRESSION, e.User, user.CurrentTicketID));
        }
    }
	// Update is called once per frame
	void Update ()
    {
		
	}
}
