﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDisplayActions : MonoBehaviour {
    public TestUIEvents UICoordUpdate;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void UIUpdate(int i, int j, int k, int l)
    {
        UICoordUpdate.Raise(i, j, k, l);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
