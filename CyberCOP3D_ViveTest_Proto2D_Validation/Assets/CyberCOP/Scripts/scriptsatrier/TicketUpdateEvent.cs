﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketUpdateEvent : MonoBehaviour {

    public CyberCopEventTickets TicketEvent;
    
    private void OnEnable()
    {
        AssetsUpdateEventListener.TicketUpdateDelegate += TicketUpdateState;
        UsersActionsListener.TicketUpdateDelegate += TicketUpdateState;
    }
    private void OnDisable()
    {
        AssetsUpdateEventListener.TicketUpdateDelegate -= TicketUpdateState;
        UsersActionsListener.TicketUpdateDelegate -= TicketUpdateState;
    }

    public void TicketUpdateState(CyberCopEventsInformation e)//traduction ici et création ticket?
    {
        TicketEvent.Raise(e, true);
    }


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
