﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioActions : MonoBehaviour { //faire le management du scenario ici ou dans un autre script??

    public AssetInfoList Scenario;
    public CyberCopEventSystem SystemEvents;


    /*faire delegate pour déclenchement des events ici??*/
    private void OnEnable()
    {
        
    }
    private void OnDisable()
    {
        
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void LaunchAttack(int i)
    {
        CyberCopEventsInformation cyber = new CyberCopEventsInformation(Scenario.GetAttack(i).assetConcerned, Scenario.GetAttack(i).cyberPhysical, (int)SystemActions.BEGINATTACK, 100, i);
        SystemEvents.Raise(cyber, true);
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            for(int i = 0; i < Scenario.Attacks.Length; i++)
            {
                LaunchAttack(i);
            }
        }
	}
}
