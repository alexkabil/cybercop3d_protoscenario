﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class IncidentTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;

    public IncidentTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner;
        this.type = type;
    }

    public IncidentTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
		this.owner = -1;
    }
}

public class IncidentUpdateEventListener : MonoBehaviour {

	public AssetInfoList Scenario;
	private List<IncidentTest> incidentList = new List<IncidentTest>();
	private CyberCopEventsInformation previousIncidentUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);
	// Use this for initialization
	void Start () 
	{
		
	}
	
	public void IncidentUpdateState(CyberCopEventsInformation e, bool b)
	{
		previousIncidentUpdate = e;
		switch(e.Action)
		{
			case (int)IncidentUpdateActions.INCIDENTCREATION:
			break;
			case (int)IncidentUpdateActions.INCIDENTSELECTION:
			break;
			case (int)IncidentUpdateActions.INCIDENTUNSELECTION:
			break;
			default:
			break;
		}
	}



	// Update is called once per frame
	void Update () 
	{
		
	}
}
