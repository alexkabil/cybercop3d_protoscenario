﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AlertTest
{
    public int id;
    public int progress;
    public bool isSelected;
    public int owner;
    public int type;
    public List<RequiredActions> ActionsPerformed;

    public AlertTest(int id, int progress, int owner,int type, bool isSelected)
    {
        this.id = id;
        this.progress = progress;
        this.isSelected = isSelected;
        this.owner = owner;
        this.type = type;
        ActionsPerformed = new List<RequiredActions>();
    }

    public AlertTest()
    {
        this.id = -1;
        this.progress = -1;
        this.isSelected = false;
        this.type = -1;
		this.owner = -1;
        ActionsPerformed = new List<RequiredActions>();
    }

    public bool IsActionAlreadyPerformed(RequiredActions action)
    {
        if (ActionsPerformed.Contains(action))
        {
            return true;
        }
        return false;
    }

    public void AddAction(RequiredActions action)
    {
        if (!ActionsPerformed.Contains(action))
        {
            ActionsPerformed.Add(action);
        }
    }
}

public class AlertUpdateEventListener : MonoBehaviour {

	public AssetInfoList Scenario;
	/*Mettre ça dans un autre script? */
	public GameObject AlertPrefab;
	public Transform AlertButtonParent;
	public Transform AlertContextualUI;
    public UnityEngine.UI.Text alertText;

    private CyberCopEventsInformation previousAlertUpdate = new CyberCopEventsInformation(999, 999, 999, 999, 999);

    public delegate void OnAlertUIUpdate(CyberCopEventsInformation e); //test delegate ui
    public static event OnAlertUIUpdate AlertUIUpdateDelegate;

    private AlertTest currentUserAlert = new AlertTest();
	private List<AlertTest> alertList = new List<AlertTest>();
	// Use this for initialization
	void Start () 
	{

	}
	
	public void AlertUpdateState(CyberCopEventsInformation e, bool b)
	{
		previousAlertUpdate =e;
		//filtrage en fonction de l'action demandée???

		switch(e.Action)
		{
			case (int)AlertUpdateActions.ALERTCREATION:
			AlertCreation(e);
			break;
			case (int)AlertUpdateActions.ALERTSELECTION:
			AlertSelection(e);
			break;
			case (int)AlertUpdateActions.ALERTUNSELECTION:
			AlertUnSelection(e);
			break;
			default:
			break;
		}
	}

	public void AlertCreation(CyberCopEventsInformation e)
	{
		alertList.Add(new AlertTest(e.TicketID,0,e.User ,0,false));//e.user=100
		//creer bouton ici? condition?
		if(!Scenario.Currentuser.isImmersive)
		{
			AlertButtonCreation(e);
		}
		Debug.Log("alerte cree");
	}
	public void AlertUnSelection(CyberCopEventsInformation e)
	{
        //raz du ticketid si jamais l'user n'a plus d'alerte selectionnee?
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var alert = (from element in alertList where element.id == e.TicketID select element).FirstOrDefault();
        if (user.CurrentTicketID==e.TicketID)
		{
			user.CurrentTicketID=-1;
		}
        alert.isSelected = false;
        AlertContextualUIUpdate(e, alert, false);
        alert.ActionsPerformed.Add(RequiredActions.ALERTUNSELECTION);//vérifier ca??

        AlertButtonSelection(e.TicketID,false);
		Debug.Log("Unselection? "+e.TicketID+" "+e.User);
	}
	public void AlertSelection(CyberCopEventsInformation e)
	{
		var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var alert = (from element in alertList where element.id == e.TicketID select element).FirstOrDefault();
		if(/* user.CurrentTicketID!=-1 && */user.CurrentTicketID != e.TicketID) //non, ce qui compte c'est la previously selected by user.
        {
           	var prevalert = (from element in alertList where element.id == user.CurrentTicketID select element).FirstOrDefault();
           	if(prevalert!=null)
			{
				prevalert.isSelected = false;
                prevalert.ActionsPerformed.Add(RequiredActions.ALERTUNSELECTION);
           		Debug.Log("deselection autre alerte");
				   AlertButtonSelection(prevalert.id, false);

                AlertContextualUIUpdate(e, alert, false);
                //envoi event alert unselection???
            }
		}
		if(alert!=null)//l'alerte existe
		{
			//filtrage en fonction du role?
			alert.isSelected = true;
            alert.progress = Scenario.GetAttack(e.TicketID).attackState;//voir si on fait une fonction pour ça
			user.CurrentTicketID = e.TicketID; //ici ticketid veut dire alertid..
			//vérifier alerte précédente pour la déselectionner par ex?
			Debug.Log("alerte selectionnee, menu contextuel maj?");
			if(!Scenario.Currentuser.isImmersive)
			{
				AlertButtonSelection(alert.id,true);
                AlertContextualUIUpdate(e, alert, true);
            }
		}
		if(user.id==Scenario.Currentuser.id) // a definir en fonction des roles pour le declenchement des event
		{
			currentUserAlert = alert;
			//mise à jour différente que si c'était le meme ticket que moi aussi??
		}
		else
		{
			//autre maj graphique sur la sélection et pas de changement de mon ui..
		}
	}

	public void AlertButtonCreation(CyberCopEventsInformation e)
	{/*rajouter conditions sur les ui? */
		var button = GameObject.Instantiate(AlertPrefab, AlertButtonParent);
	    button.GetComponent<ButtonAlertDelegateAction>().SetButtonInitState(e);	
	    button.transform.SetAsFirstSibling();
	    AlertButtonParent.GetComponent<RectTransform>().sizeDelta+= new Vector2(0,button.GetComponent<RectTransform>().sizeDelta.y);
	}

	public void AlertButtonSelection(int ticketid,bool b)
	{
		var alert =(from item in AlertButtonParent.GetComponentsInChildren<ButtonAlertDelegateAction>() where item.ticketID==ticketid select item).FirstOrDefault();
		if(alert!=null)
		{
			alert.GetComponent<ButtonHighlighter>().Highlight(b);
			if(!b)
			{
				alert.GetComponent<ButtonAlertDelegateAction>().ResetActionUnselection();
			}
		}
	}
    /*dans un premier temps on teste ici puis on délèguera...*/
	public void AlertContextualUIUpdate(CyberCopEventsInformation e, AlertTest alert, bool b)
    {
        AlertUIUpdateDelegate(e);
        /*
        if (!b)
        {
            alertText.text = "";
            return;
        }
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        var attack = Scenario.GetAttack(e.TicketID);
        string currentAction = "Next action: " + System.Enum.GetName(typeof(RequiredActions), attack.GetCurrentAction())+" \n";
        string alertState = "Alert ID : " + e.TicketID+ "   user: "+e.User+"  \n";
        foreach(RequiredActions re in attack.ActionsPerformed)
        {
            alertState += " done :" + System.Enum.GetName(typeof(RequiredActions), re) + "\n";
        }
        alertText.text = currentAction + alertState;
        */
    }

	// Update is called once per frame
	void Update () 
	{
		
	}
}
