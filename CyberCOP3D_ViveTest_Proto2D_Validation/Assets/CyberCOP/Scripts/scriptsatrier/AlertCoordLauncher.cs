﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertCoordLauncher : MonoBehaviour
{
    public TestUIEvents AlertCoord;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void AlertEvent(int i, int j, int k, int l)
    {
        AlertCoord.Raise(i, j, k, l);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
