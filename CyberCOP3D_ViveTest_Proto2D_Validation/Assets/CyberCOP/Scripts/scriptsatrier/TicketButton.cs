﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketButton : MonoBehaviour
{
    private int _buttonId=-1;
    private int _alertId=-1;
    private int _ticketType=-1;
    private int _user = -1;
    private int _actionrequired = -1;
    private bool _init=true;

    public TicketButtonClicked ButtonClick;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ClickButton()
    {
        if (_init)
        {
            ButtonClick.ButtonClicked(_buttonId, _ticketType, _actionrequired, _user);
            //_init = false;
        }
        
    }

    public void SetButtonState(int i, int j, int k, int l)
    {
        _buttonId = i;
        _ticketType = j;
        _actionrequired = k;
        _user = l;
    }

}
