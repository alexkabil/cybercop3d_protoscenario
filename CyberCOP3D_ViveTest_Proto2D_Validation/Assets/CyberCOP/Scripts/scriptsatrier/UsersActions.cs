﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UsersActions : MonoBehaviour {
    public CyberCopEventUsers UserEvent;
    public AssetInfoList Scenario;
	// Use this for initialization
	void Start ()
    {
		
	}
    private void OnEnable()//l'inconvénient c'est que quand on instancie les boutons en RT ca marche pas... ou alors laisser le delegate et lier les boutons avec
    {
        ButtonAssetDelegateAction.ButtonDelegate += AssetButtonSelection;
        ButtonAlertDelegateAction.ButtonDelegate += AlertButtonAction;

        AlertActionsButtons.ButtonDelegate += AlertContextualButtonAction;
    }
    //juste filtrage en fonction de l'asset selection et de la vue? mise d'accord sur 1=kinetic et 0=cyber?
    public void AssetButtonSelection(CyberCopEventsInformation e)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        UserEvent.Raise(new CyberCopEventsInformation(e.Asset,e.View,e.Action,user.id,user.CurrentTicketID),true);
    }

    public void AlertButtonAction(CyberCopEventsInformation e)
    {
        //UserEvent.Raise(new CyberCopEventsInformation(e.Asset,e.View,e.Action,e.User,e.TicketID),true);// autant passer e nan?
        UserEvent.Raise(e, true);
    }

    public void AlertContextualButtonAction(CyberCopEventsInformation e) //envoi action coord à analyste (creation ticket)
    {
        UserEvent.Raise(e,true);
    }

    private void OnDisable()
    {
        ButtonAssetDelegateAction.ButtonDelegate -= AssetButtonSelection;
        ButtonAlertDelegateAction.ButtonDelegate -= AlertButtonAction;

        AlertActionsButtons.ButtonDelegate -= AlertContextualButtonAction;
    }
    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            UserEvent.Raise(new CyberCopEventsInformation(4, 0, (int)RequiredActions.ALERTSELECTION_ANALYST, 1, 0), true);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            UserEvent.Raise(new CyberCopEventsInformation(5, 1, (int)RequiredActions.ALERTSELECTION_ANALYST, 1, 1), true);
        }
	}
}
