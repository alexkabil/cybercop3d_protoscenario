﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ButtonHighlighter : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void Highlight(bool b)
    {
        GetComponent<UnityEngine.UI.Image>().color = b==true ? new Color(0.3f,1, 0.3f) : new Color(1,1,1);
    }
    public void OtherHighlight(bool b)
    {
        var high = gameObject.GetComponentsInChildren<UnityEngine.UI.Image>(); //le pere est pris en compte donc faut prendre le 2e
        if (high.Length>1)
        {
            Debug.Log(high[1].gameObject.name);
            high[1].enabled = b;
        }
    }

    public void AlertText(bool b)
    {
        var uiText = gameObject.GetComponentInChildren<UnityEngine.UI.Text>();
        uiText.color = b==true? Color.red:Color.black;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
