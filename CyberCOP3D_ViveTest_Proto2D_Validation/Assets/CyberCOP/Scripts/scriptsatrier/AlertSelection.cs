﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AlertSelection : MonoBehaviour
{
    public AssetInfoList Scenario;
    /*test de mise truc scénar ici plutot que sur le launcher*/
    public RequiredActions ScenarioAction = RequiredActions.ALERTSELECTION;
    //public TestUIEvents CoordUIDisplay;
    public UIDisplayActions UIDisplay;
    // Use this for initialization
    void Start()
    {

    }
    
    public void AlertSelect(int i, int j, int k, int l)
    {
        //Scenario.SetCurrentAttack(j); //un seul cas à la fois??
        Scenario.CheckScenarioAction(ScenarioAction, j);
        //On check l'état du scénario en fonction de l'alerte
        Scenario.SetCurrentAttack(j);
        UIDisplay.UIUpdate(i, j, (int)ScenarioAction, Scenario.Currentuser.id);
        //CoordUIDisplay.Raise(i, j, (int)ScenarioAction, Scenario.Currentuser.id); //on passe l'alerte et son état.. on laisse comme ca pour l'instant...
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
