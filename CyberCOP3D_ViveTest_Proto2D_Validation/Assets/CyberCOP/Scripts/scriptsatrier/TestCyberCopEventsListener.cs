﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCyberCopEventsListener : MonoBehaviour {
    public CyberCopEvent even;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void GetEvent(CyberCopEventsInformation i, bool b)
    {
        Debug.Log(i.Asset + " " + i.View + " " + i.Action + " " + i.User + " " + i.TicketID + " ");
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            even.Raise(new CyberCopEventsInformation(1, 2, 3, 4, 5), true);
            Debug.Log("aaa ");
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            even.Raise();
            Debug.Log("bbb");
        }
    }
}
