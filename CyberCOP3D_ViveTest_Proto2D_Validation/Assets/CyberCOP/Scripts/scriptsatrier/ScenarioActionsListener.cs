﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioActionsListener : MonoBehaviour {

    public delegate void OnAssetStateUpdate(CyberCopEventsInformation e);
    public static event OnAssetStateUpdate AssetUpdateDelegate;
    private CyberCopEventsInformation previousSystemAction = new CyberCopEventsInformation(999, 999, 999, 999, 999);
    
    // Use this for initialization
    void Start ()
    {
		
	}

    public void ScenarioActionsReceived(CyberCopEventsInformation e, bool b)
    {
        if (previousSystemAction.Action == e.Action && previousSystemAction.Asset == e.Asset && previousSystemAction.TicketID == e.TicketID && previousSystemAction.User == e.User && previousSystemAction.View == e.View)
        {
            Debug.Log("meme event systeme");
            return;
        }
        previousSystemAction = e;

        //filtrage par utilisateur d'abord? 0==me filtrage en fonction du scénario?? pas pour le scenario? 0 veut ptet dire (systeme?) user = 100?
        if (e.User == 100)
        {
            //filtrage par action?
            switch (e.Action)
            {
                case (int)SystemActions.BEGINATTACK:
                    //changement action user vers action asset 
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ADDMALICIOUSPROCESSES, e.User, e.TicketID));
                    break;
                case (int)SystemActions.BEGINFALSEATTACK:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ADDLEGITPROCESSES, e.User, e.TicketID));
                    break;
                default:
                    break;
            }
        }
        else //a voir ce qu'on fait..
        {
            switch (e.Action)
            {
                case (int)SystemActions.BEGINATTACK:
                    //changement action user vers action asset 
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    break;
                case (int)SystemActions.BEGINFALSEATTACK:
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.METRICRAISE, e.User, e.TicketID));
                    AssetUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AssetUpdateActions.ALERT, e.User, e.TicketID));
                    break;
                default:
                    break;
            }
        }
    }


    // Update is called once per frame
    void Update ()
    {
		
	}
}
