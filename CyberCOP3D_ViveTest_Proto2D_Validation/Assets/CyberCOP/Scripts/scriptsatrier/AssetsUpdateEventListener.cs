﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AssetsUpdateEventListener : MonoBehaviour {
    public AssetInfoList Scenario;
    public Transform CyberAssetsFather;
    public Transform KineticAssetsFather;

    public delegate void OnTicketStateUpdate(CyberCopEventsInformation e);
    public static event OnTicketStateUpdate TicketUpdateDelegate;

    public delegate void OnAssetUIUpdate(CyberCopEventsInformation e);
    public static event OnAssetUIUpdate UIUpdateDelegate;

    public delegate void OnAlertStateUpdate(CyberCopEventsInformation e);
    public static event OnAlertStateUpdate AlertUpdateDelegate;

    private CyberCopEventsInformation previousAssetUpdate = new CyberCopEventsInformation(999,999,999,999,999);
    // Use this for initialization
    void Start ()
    {
		
	}
	
    public void AssetUpdateState(CyberCopEventsInformation e, bool b)
    {
        if(previousAssetUpdate.Action==e.Action && previousAssetUpdate.Asset==e.Asset && previousAssetUpdate.TicketID == e.TicketID && previousAssetUpdate.User == e.User && previousAssetUpdate.View == e.View)
        {
            Debug.Log("meme event asset");
            return;
        }
        previousAssetUpdate = e;
        UIUpdateDelegate(e);
        switch (e.Action)
        {
            //filtrage avec l'enum des assets ici wesh
            case (int)AssetUpdateActions.ASSETSELECTION:
                AssetSelectionType(e, true);
                break;
            case (int)AssetUpdateActions.ASSETUNSELECTION:
                AssetSelectionType(e, false);
                break;
            case (int)AssetUpdateActions.ALERT:
                AssetAlert(e);
                break;
            case (int)AssetUpdateActions.METRICRAISE:
                ValueRaise(e);
                break;
            case (int)AssetUpdateActions.ADDMALICIOUSPROCESSES:
                Debug.Log("ajout processes malicieux");
                break;
            default:
                break;
        }
    }


    public void ValueRaise(CyberCopEventsInformation e)
    {
        if (e.View == 0)
        {
            Debug.Log("hausse net");
        }
        if (e.View == 1)
        {
            Debug.Log("hausse entropie");
        }
    }

    public void AssetAlert(CyberCopEventsInformation e)
    {
        if (e.User == 100)//system?
        {
            if (e.View == 0) //cyber
            {
                var asset = (from element in CyberAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).First();
                if (asset != null)
                {
                    if(Scenario.Assets[e.Asset - 1].State.cyberState == AssetStatesInformation.assetTypeStates.NORMAL)
                    {

                        Scenario.Assets[e.Asset - 1].State.cyberState = AssetStatesInformation.assetTypeStates.ALERT;
                        asset.GetComponent<ButtonHighlighter>().AlertText(true);
                    }
                }
                AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTCREATION, e.User, e.TicketID));
                return;
            }
            if (e.View == 1) //kinetic
            {
                var asset = (from element in KineticAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).First();
                if (asset != null)
                {
                    if (Scenario.Assets[e.Asset - 1].State.kineticState == AssetStatesInformation.assetTypeStates.NORMAL)
                    {
                        Scenario.Assets[e.Asset - 1].State.kineticState = AssetStatesInformation.assetTypeStates.ALERT;
                        asset.GetComponent<ButtonHighlighter>().AlertText(true);
                    }
                }
                AlertUpdateDelegate(new CyberCopEventsInformation(e.Asset, e.View, (int)AlertUpdateActions.ALERTCREATION, e.User, e.TicketID));
                return;
            }
            return;
        }
        else
        {
            return;
        }
    }

    public void AssetSelectionType(CyberCopEventsInformation e, bool b)
    {
        var user = (from item in Scenario.Users where item.id == e.User select item).FirstOrDefault();
        
        if (user==Scenario.Currentuser) //own user? avec id?
        {

            //rajouter filtrage en fonction du role? faire une fonction pour 3D ou non.
            if(!Scenario.Currentuser.isImmersive)
            {
                if (e.View==0)//cyber
                {
                    var asset = (from element in CyberAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).FirstOrDefault();
                    if (asset != null)
                    {
                        if (Scenario.Assets[e.Asset - 1].State.cyberState == AssetStatesInformation.assetTypeStates.NORMAL)  //rajouter conditions sur l'état asset..
                        {
                            asset.GetComponent<ButtonHighlighter>().Highlight(b);
                            return;
                        }
                        if (Scenario.Assets[e.Asset - 1].State.cyberState == AssetStatesInformation.assetTypeStates.ALERT)  //rajouter conditions sur l'état asset..
                        {
                            asset.GetComponent<ButtonHighlighter>().Highlight(b);
                            return;
                        }
                    }
                }

                if (e.View == 1)//kinetic
                {
                    var asset = (from element in KineticAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).FirstOrDefault();
                    if (asset != null)
                    {
                        if (Scenario.Assets[e.Asset - 1].State.kineticState == AssetStatesInformation.assetTypeStates.NORMAL)  //rajouter conditions sur l'état asset..
                        {
                            asset.GetComponent<ButtonHighlighter>().Highlight(b);
                            return;
                        }
                        if (Scenario.Assets[e.Asset - 1].State.kineticState == AssetStatesInformation.assetTypeStates.ALERT)  //rajouter conditions sur l'état asset..
                        {
                            asset.GetComponent<ButtonHighlighter>().Highlight(b);
                            return;
                        }
                    }
                }                
            }

        }
        else
        {
            Debug.Log("Autre user");
            //separation 2D 3D
            if(!Scenario.Currentuser.isImmersive)
            {
                if (e.View == 0)//cyber
                {
                    var asset = (from element in CyberAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).FirstOrDefault();
                    if (asset != null)
                    {
                        asset.GetComponent<ButtonHighlighter>().OtherHighlight(b);
                        return;
                    }
                }
                if (e.View == 1)//kinetic
                {
                    var asset = (from element in KineticAssetsFather.GetComponentsInChildren<ButtonAssetDelegateAction>() where element.butID == e.Asset select element).First();
                    if (asset != null)
                    {
                        asset.GetComponent<ButtonHighlighter>().OtherHighlight(b);
                        return;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
