﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsUpdateEvent : MonoBehaviour {
    public CyberCopEventAssets AssetEvent;
    void Start()
    {

    }
    private void OnEnable()
    {
        UsersActionsListener.AssetUpdateDelegate+= AssetUpdateState;
        ScenarioActionsListener.AssetUpdateDelegate += AssetUpdateState;
    }
    
    public void AssetUpdateState(CyberCopEventsInformation e)
    {
        Debug.Log("asset update : " + e.Asset + System.Enum.GetName(typeof(AssetUpdateActions), e.Action));
        //traduction de l'evenement??
        AssetEvent.Raise(e, true);
    }

    private void OnDisable()
    {
        UsersActionsListener.AssetUpdateDelegate -= AssetUpdateState;
        ScenarioActionsListener.AssetUpdateDelegate -= AssetUpdateState;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
