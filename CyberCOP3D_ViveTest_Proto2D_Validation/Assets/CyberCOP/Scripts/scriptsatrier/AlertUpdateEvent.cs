﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertUpdateEvent : MonoBehaviour {

	public CyberCopEvent AlertEvent;

	private void OnEnable() 
	{
		AssetsUpdateEventListener.AlertUpdateDelegate+=AlertUpdateState;	
		UsersActionsListener.AlertUpdateDelegate+=AlertUpdateState;
	}
	private void OnDisable() 
	{
		AssetsUpdateEventListener.AlertUpdateDelegate-=AlertUpdateState;
		UsersActionsListener.AlertUpdateDelegate-=AlertUpdateState;
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	public void AlertUpdateState(CyberCopEventsInformation e)
	{
		Debug.Log("delegate recu");
		//if(e.Action==(int)AlertUpdateActions.ALERTCREATION)
		//{
			AlertEvent.Raise(e, true);
		//}
		
		//filtrer ici informations ?
		//AlertEvent.Raise(e, true);
	}

	// Update is called once per frame
	void Update () {

		
	}
}
