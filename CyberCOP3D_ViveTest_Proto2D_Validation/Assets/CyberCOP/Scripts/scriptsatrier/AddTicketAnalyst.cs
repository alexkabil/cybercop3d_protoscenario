﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTicketAnalyst : MonoBehaviour {
    public GameObject prefab;
    public Transform ticketparent;
	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void CreateTicket(int i, int j, int k, int l)
    {
        var button = GameObject.Instantiate(prefab, ticketparent);
        button.transform.SetAsFirstSibling();
        ticketparent.GetComponent<RectTransform>().sizeDelta += new Vector2(0,button.GetComponent<RectTransform>().sizeDelta.y);
        button.GetComponent<TicketButton>().SetButtonState(i, j, k, l);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
