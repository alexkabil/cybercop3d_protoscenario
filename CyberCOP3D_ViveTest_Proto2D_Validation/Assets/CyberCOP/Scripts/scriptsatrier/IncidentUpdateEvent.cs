﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncidentUpdateEvent : MonoBehaviour {

	public CyberCopEvent IncidentEvent;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	public void IncidentUpdateState(CyberCopEventsInformation e)
	{
		IncidentEvent.Raise(e,true);
	}

	// Update is called once per frame
	void Update () 
	{
		
	}
}
