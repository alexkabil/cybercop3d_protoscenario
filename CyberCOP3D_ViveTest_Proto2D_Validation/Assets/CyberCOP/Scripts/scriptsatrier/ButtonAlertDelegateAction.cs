﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ButtonAlertDelegateAction : MonoBehaviour,IPointerClickHandler {
    public delegate void OnButtonClickDelegate(CyberCopEventsInformation e);
    public static event OnButtonClickDelegate ButtonDelegate;
    private int butID=0;
    private int isKinetic = 0;
    private int _action =0;
    public int ticketID=-1;
    private bool isSelected = false;
    private int _user = 1;// en fonction du current user id?
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void ResetActionUnselection()
    {
        _action = 0;
    }
    public void SetButtonInitState(CyberCopEventsInformation e)
    {
        butID = e.Asset;
        isKinetic = e.View;
        ticketID = e.TicketID;
        //_user= e.User;
        string s = isKinetic==0?"network issue": "high entropy";
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Alert Asset " + butID + " "+s;
    }
     
    public void OnPointerClick(PointerEventData eventData)
    {
       // Debug.Log(eventData.selectedObject.name);
        _action++;
        int act = _action % 2 == 0 ? (int)RequiredActions.ALERTUNSELECTION : (int)RequiredActions.ALERTSELECTION;
        ButtonDelegate(new CyberCopEventsInformation(butID,isKinetic,act,_user,ticketID));
        Debug.Log("action "+ System.Enum.GetName(typeof(RequiredActions),(RequiredActions)act) + " id "+ticketID);
        //throw new System.NotImplementedException();
    }
}
