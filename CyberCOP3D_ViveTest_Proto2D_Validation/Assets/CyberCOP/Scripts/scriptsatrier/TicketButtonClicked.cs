﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketButtonClicked : MonoBehaviour
{
    public TestUIEvents TicketSelected;
    private int _asset=-1;
    private int _alertid = -1;
    private int _action = -1;
    private int _user = -1;

	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void ButtonClicked(int i, int j, int k, int l)
    {
        if(i==_asset && j==_alertid && k==_action && l == _user) //on reclique sur le bouton
        {
            return;
        }
        _asset = i;
        _alertid = j;
        _action = k;
        _user = l;
        //mettre à jour le ticket courant de l'analyste?
        TicketSelected.Raise(i, j, k, l);
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
