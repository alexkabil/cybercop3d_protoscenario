﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VizRoomUI : MonoBehaviour {
    public AssetInfoList Scenario;
    public int selectedAsset = -1;
    public UnityEngine.UI.Text AssetText;
    public UnityEngine.UI.Text AssetName;
    // Use this for initialization
	void Start ()
    {
		
	}
    public void IncrementDisplay()
    {
        selectedAsset++;
        if (selectedAsset>=0 && selectedAsset < Scenario.Assets.Count)
        {
            AssetText.text = Scenario.Assets[selectedAsset].DataInfo();
            AssetName.text = "Asset " + selectedAsset.ToString();
        }
    }
    public void DecrementDisplay()
    {
        selectedAsset--;
        if (selectedAsset >= 0 && selectedAsset<Scenario.Assets.Count)
        {
            AssetText.text = Scenario.Assets[selectedAsset].DataInfo();
            AssetName.text = "Asset " + selectedAsset.ToString();
        }
    }
    // Update is called once per frame
    void Update ()
    {
		
	}
}
