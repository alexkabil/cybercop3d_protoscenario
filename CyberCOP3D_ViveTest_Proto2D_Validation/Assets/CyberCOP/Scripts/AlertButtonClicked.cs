﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertButtonClicked : MonoBehaviour
{
    private int _alertbuttonstate = -1;
    private int _previousalertbuttonstate = -2;
    public TestUIEvents AlertSelection;
    //public TestUIEvents CoordUIDisplayEvent;
    //public TestUIEvents CoordUIResetEvent;
    public AssetInfoList Scenario;

	// Use this for initialization
	void Start ()
    {
	}
	
    public void SetAlertButtonState(int i)
    {
        _previousalertbuttonstate = _alertbuttonstate;
        _alertbuttonstate = i;
        //Scenario.SetCurrentAttack(i);
    }

    public void ClickButton(int i, int j, int k, int l)
    {
        if (l == Scenario.Currentuser.id)
        {
            SetAlertButtonState(j);
            if (_previousalertbuttonstate == _alertbuttonstate) //meme bouton recliqué
            {
                Debug.Log("meme bouton d'alerte cliqué");
                return;
            }
            else
            {
                AlertSelection.Raise(i, j, k, l); //ici l=0 donc coord
            }
        }
        

    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
