﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
public class CyberCOPVRTKButton : MonoBehaviour {
    public VRTK_ControllerEvents Gamepad;
    public UIEventBoolean MenuEventMove;
    private bool isUIActive = false;
    public bool isVive = false;
    private void OnEnable()
    {
        if (Gamepad != null)
        {
            Gamepad.ButtonOnePressed += DoButtonOnePressed;
            Gamepad.ButtonOneReleased += DoButtonOneReleased;
            //controllerEvents.ButtonOneTouchStart += DoButtonOneTouchStart;
            //controllerEvents.ButtonOneTouchEnd += DoButtonOneTouchEnd;

            Gamepad.ButtonTwoPressed += DoButtonTwoPressed;
            Gamepad.ButtonTwoReleased += DoButtonTwoReleased;
            //controllerEvents.ButtonTwoTouchStart += DoButtonTwoTouchStart;
            //controllerEvents.ButtonTwoTouchEnd += DoButtonTwoTouchEnd;
        }
    }

    private void OnDisable()
    {
        if (Gamepad != null)
        {
            Gamepad.ButtonOnePressed -= DoButtonOnePressed;
            Gamepad.ButtonOneReleased -= DoButtonOneReleased;
            //controllerEvents.ButtonOneTouchStart -= DoButtonOneTouchStart;
            //controllerEvents.ButtonOneTouchEnd -= DoButtonOneTouchEnd;

            Gamepad.ButtonTwoPressed -= DoButtonTwoPressed;
            Gamepad.ButtonTwoReleased -= DoButtonTwoReleased;
            //controllerEvents.ButtonTwoTouchStart -= DoButtonTwoTouchStart;
            //controllerEvents.ButtonTwoTouchEnd -= DoButtonTwoTouchEnd;
        }
    }
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}


    private void DoButtonOnePressed(object sender, ControllerInteractionEventArgs e)
    {
        isUIActive = !isUIActive;
        if (isUIActive)
        {
            MenuEventMove.Raise(true);
        }
        else
        {
            MenuEventMove.Raise(false);
        }
    }

    private void DoButtonOneReleased(object sender, ControllerInteractionEventArgs e)
    {
        
    }


    private void DoButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
    {
        if (isVive)
        {
            isUIActive = !isUIActive;
            if (isUIActive)
            {
                MenuEventMove.Raise(true);
            }
            else
            {
                MenuEventMove.Raise(false);
            }
        }
    }

    private void DoButtonTwoReleased(object sender, ControllerInteractionEventArgs e)
    {
        
    }

    
}
