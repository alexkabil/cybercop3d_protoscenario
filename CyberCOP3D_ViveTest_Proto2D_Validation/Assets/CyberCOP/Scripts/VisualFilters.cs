﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualFilters : MonoBehaviour {
    private GameObject[] _PhysicalObjects;
    private GameObject[] _CyberObjects;
    // Use this for initialization
	void Start ()
    {
        _PhysicalObjects = GameObject.FindGameObjectsWithTag("3DObj");
        _CyberObjects = GameObject.FindGameObjectsWithTag("TopologyItem");
    }

    public void AssetActivation(int i)
    {
        if (i == 0)
        {
            foreach(GameObject g in _PhysicalObjects)
            {
                g.SetActive(true);
            }
            foreach (GameObject g in _CyberObjects)
            {
                g.SetActive(false);
            }
        }
        else if (i == 1)
        {
            foreach (GameObject g in _PhysicalObjects)
            {
                g.SetActive(false);
            }
            foreach (GameObject g in _CyberObjects)
            {
                g.SetActive(true);
            }
        }
    }

    public void CyberPhysicalView(int i)
    {
        switch (i)
        {
            case 0:
                Camera.main.cullingMask = -39955;

                break;
            case 1:
                //Camera.main.cullingMask = -63763;
                Camera.main.cullingMask = -64275;
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update ()
    {

    }
}
