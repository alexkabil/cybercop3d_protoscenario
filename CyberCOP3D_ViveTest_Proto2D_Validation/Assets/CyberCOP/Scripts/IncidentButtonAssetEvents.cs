﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncidentButtonAssetEvents : MonoBehaviour
{
    private int _assetId=0;
    private int _state=0;
    private int _alertType=0;
    public AssetInfoList Scenario;

    public TestUIEvents EventIncidentAnalyst;

	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void AnalystDeclareIncidentAlertAsset(int i, int j, int k, int l)
    {
        if (i != _assetId)
        {
            Debug.Log("pas le bon asset");
            return;
        }
        else
        {
            if (Scenario.Assets[i - 1].State.isAlert /*&& Scenario.Assets[i - 1].State.isSelectedCoord*/)
            {
                EventIncidentAnalyst.Raise(i, j, k, l);

            }
            else //état inconnu?
            {
                Debug.Log("Pas le bon état d'asset");
                return;
            }
            
        }
    }

    public void SetInformationIncident(int i, int j, int k, int l)
    {
        _assetId = i;
        _state = j;
        _alertType = k;
    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
