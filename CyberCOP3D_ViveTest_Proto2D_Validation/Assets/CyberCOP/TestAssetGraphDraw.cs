﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
public class TestAssetGraphDraw : MonoBehaviour {
    public GraphChart graph;
    public FloatAsset Asset;
    public GameEventSO EventAsset1;
    public bool isSelected = false;
    public Dictionary<int, float> prevValues;
    private int index = 0;
    private bool isDraw = true;
    // Use this for initialization
	void Start ()
    {
        prevValues = new Dictionary<int, float>();
        prevValues.Add(0, 5.0f);
        prevValues.Add(1, 50.0f);
        prevValues.Add(2, 200.0f);
        prevValues.Add(3, -20.0f);
        prevValues.Add(4, -10.0f);
        prevValues.Add(5, 5.0f);
        prevValues.Add(6, 50.0f);
        prevValues.Add(7, 100.0f);
        prevValues.Add(8, -20.0f);
        prevValues.Add(9, -10.0f);
        prevValues.Add(10, 5.0f);
        prevValues.Add(11, 50.0f);
        prevValues.Add(12, -200.0f);
        prevValues.Add(13, -20.0f);
        prevValues.Add(14, -10.0f);
        prevValues.Add(15, 0);
        prevValues.Add(16, 0);
        index = prevValues.Count + 1;
        DrawGraph();
        StartCoroutine(GraphDrawTiming(1.0f));
	}
    private void OnEnable()
    {
        Debug.Log("aaa");
    }
    private void OnDisable()
    {
        isDraw = false;
        Debug.Log("deActivation");
    }
    public void DrawGraph()
    {
        index++;
        if (graph != null)
        {
            graph.DataSource.StartBatch();
            graph.DataSource.ClearCategory("Entropy");
            if (prevValues != null)
            {
                foreach (var pair in prevValues)
                {
                    graph.DataSource.AddPointToCategory("Entropy", (double)pair.Key, (double)pair.Value);
                }
            }

            graph.DataSource.EndBatch();
        }
    }

    public void AddGraphVal(float f)
    {
        index++;
        prevValues.Add(index, f);
        if (prevValues.Count > 21)
        {
            prevValues.Remove(0);
        }
        if (isSelected)
        {
            graph.DataSource.StartBatch();
            graph.DataSource.AddPointToCategoryRealtime("Entropy", (double)index, (double)f);
            graph.DataSource.EndBatch();
        }
        
    }

    IEnumerator GraphDrawTiming(float f)
    {
        while (isDraw)
        {
            Asset.Value = Random.Range(-50.0f, 20.0f);
            EventAsset1.Raise(Asset.Value);

            yield return new WaitForSeconds(f);
        }
        yield return null;
    }

    public void SelectAsset()
    {
        isSelected = true;
        DrawGraph();
    }
    public void UnselectAsset()
    {
        isSelected = false;
        DrawGraph();
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SelectAsset();
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            UnselectAsset();
        }
    }
}
