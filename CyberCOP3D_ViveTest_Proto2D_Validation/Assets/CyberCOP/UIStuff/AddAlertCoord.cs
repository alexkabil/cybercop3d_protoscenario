﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddAlertCoord : MonoBehaviour
{
    public GameObject buttonPrefab;
    public Transform AlertButtonParent;
    public Transform AssetOnMap;
    public AssetInfoList Scenario;

	// Use this for initialization
	void Start ()
    {
		
	}
	
    public void SetMapAlertColor(int i, int j, int k, int l)
    {
        if (k == 0)
        {
            AssetOnMap.GetChild(i - 1).GetComponent<Image>().color = Color.red;
        }
        else if (k == 1)
        {
            AssetOnMap.GetChild(i - 1).GetComponent<Image>().color = new Color(1.0f,0,1.0f);
        }

    }

    public void CreateAlert(int i, int j, int k, int l)
    {
        var button = GameObject.Instantiate(buttonPrefab, AlertButtonParent);
        button.GetComponent<AlertButton>().SetButtonState(i,j,k,l);

        button.transform.SetAsFirstSibling();

        AlertButtonParent.GetComponent<RectTransform>().sizeDelta+= new Vector2(0,button.GetComponent<RectTransform>().sizeDelta.y);
        Scenario.alertNumber++;
        //Color col = button.GetComponent<Button>().colors.normalColor;
        //col = Color.red;
        //button.GetComponent<Button>().colors.normalColor = col;
    }


	// Update is called once per frame
	void Update ()
    {
		
	}
}
