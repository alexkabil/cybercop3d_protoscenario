﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertButton : MonoBehaviour {
    public static int classButtonID=-1;

    public int buttonID = 0;
    private int _alertid = 0;
    public int numclick = 0;
    private int _alerttype;
    private int alertState = 0;
    public AssetInfoList Scenario;//test de ref sur le scenario dans le bouton..
    public AlertButtonClicked AlertButtonClicked;

	// Use this for initialization
	void Start ()
    {
        classButtonID++;
        buttonID = classButtonID;
        AlertButtonClicked = GameObject.Find("AlertButtonClicked").GetComponent<AlertButtonClicked>();
	}
    public void ClickButton()
    {
        numclick++;
        /*
        if (numclick == 1)//chgt condition
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.8f, 0.1f, 0.5f);
        }
        */
        AlertButtonClicked.ClickButton(_alertid, buttonID, _alerttype, Scenario.Currentuser.id); //on met 0 pour coord?
        /*raise event pour texte et boutons*/
    }

    public void SetButtonState(int i,int j, int k, int l)
    {
        _alertid = i;
        buttonID = l;
        if (k == 0)
        {
            transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Alert Asset " + i + " : high entropy";
        }
        if (k == 1)
        {
            transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Alert Asset " + i + " : Network Anomalies";
        }
        _alerttype = k;
        alertState = j;

    }

	// Update is called once per frame
	void Update ()
    {
		
	}
}
