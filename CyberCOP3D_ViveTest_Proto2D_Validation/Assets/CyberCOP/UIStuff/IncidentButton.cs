﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncidentButton : MonoBehaviour
{
    private int _incidentId = 0;
    public int numclick = 0;
    private int _alerttype;
    public IncidentListClicked IncidentButtonClicked;
    // Use this for initialization
    void Start()
    {
        IncidentButtonClicked = GameObject.Find("IncidentListClicked").GetComponent<IncidentListClicked>();
    }
    public void ClickButton()
    {
        numclick++;
        IncidentButtonClicked.ClickButton(_incidentId, _alerttype, numclick, 0);
        if (numclick == 1)
        {
            GetComponent<UnityEngine.UI.Image>().color = new Color(0.1f, 0.8f, 0.1f, 0.5f);
        }
        /*raise event pour texte et boutons*/
    }

    public void SetButtonState(int i, int j)
    {
        /*remove state for now*/
        _incidentId = i;
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "Incident Asset " + i + " :";
        _alerttype = j;
        GetComponent<UnityEngine.UI.Button>().interactable = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
