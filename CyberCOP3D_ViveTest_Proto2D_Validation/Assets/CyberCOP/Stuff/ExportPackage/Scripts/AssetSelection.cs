﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetSelection : MonoBehaviour {
    public GameObject[] Assets;
    public GameObject selected= null;
	// Use this for initialization
	void Start ()
    {
		
	}

    public void DeactivateAsset()
    {
        if (selected != null)
        {
            selected.GetComponent<GazeInteractionReceiver>().UnSelect();
            selected = null;
        }
    }

    public void ActivateAsset(GameObject g)
    {
        if (selected != null)
        {
            if (g == selected)
            {
                return;
            }
            else
            {
                selected.GetComponent<GazeInteractionReceiver>().UnSelect();
            }
        }
        selected = g;
    }

	// Update is called once per frame
	void Update ()
    {
	}
}
