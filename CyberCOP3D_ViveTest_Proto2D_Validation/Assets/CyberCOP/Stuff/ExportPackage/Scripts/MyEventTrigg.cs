﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyEventTrigg : EventTrigger{

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.clickCount == 2)
        {
            base.OnDeselect(eventData);
        }
        else
        {
            base.OnPointerClick(eventData);
        }
    }

}
