﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkActions : MonoBehaviour {
    public ParticleSystem[] Topology;
    public ParticleSystem[] Map;
    public EventsList Events;

    // Use this for initialization
    void Start () {
		
	}
	
    public void SendNetInfo(string s)
    {

    }

    public void StartSound(int i)
    {
        Events.Events[i].Raise();
    }

    public void StartParticleTopology(int i)
    {
        Topology[i].Play();
        Debug.Log("topo lance "+i);
    }

    public void StopParticleTopology(int i)
    {
        Topology[i].Stop();
        Debug.Log("topo stopee "+i);
    }

    public void StartParticleMap(int i)
    {
        Debug.Log("map lancee" + i);
        Map[i].Play();
    }

    public void StopParticleMap(int i)
    {
        Debug.Log("map stoppee" + i);
        Map[i].Stop();
    }


    // Update is called once per frame
    void Update () {
		
	}
}
