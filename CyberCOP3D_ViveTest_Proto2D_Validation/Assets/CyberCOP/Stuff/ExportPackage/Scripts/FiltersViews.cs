﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiltersViews : MonoBehaviour {
    public Camera maincam;
    public bool switchview = true;
    private bool filteredview = true;
    public Material assets;
    public Material floors;
    public Material others;

    public Color assetcolorI;
    public Color assetcolorF;

    private Color floorsColorI;
    private Color floorsColorF;

    public Color othersColorI;
    public Color othersColorF;

    public AssetSelection AssetManager;


    private bool actioninProgress = false;
    private int filtermode = 0;
    // Use this for initialization
    void Start ()
    {
        assets.color = assetcolorI;
        others.color = othersColorI;
        StartCoroutine(SetTrackingHeigh());
        Debug.Log(maincam.cullingMask);
    }
	
    private IEnumerator highlight(float f)
    {
        actioninProgress = true;
        float t = 0;
        while(t<f)
        {
            assets.color = Vector4.Lerp(assets.color, assetcolorF, Time.deltaTime);
            others.color = Vector4.Lerp(others.color, othersColorF, Time.deltaTime);
            t += Time.deltaTime;
            yield return null;
        }
        others.color = othersColorF;
        assets.color = assetcolorF;
        actioninProgress = false;
        yield return null;
    }

    private IEnumerator normalcol(float f)
    {
        actioninProgress = true;
        float t = 0;
        while (t < f)
        {
            assets.color = Vector4.Lerp(assets.color, assetcolorI, Time.deltaTime*2.0f);
            others.color = Vector4.Lerp(others.color, othersColorI, Time.deltaTime*2.0f);
            t += Time.deltaTime;
            yield return null;
        }
        others.color = othersColorI;
        assets.color = assetcolorI;
        actioninProgress = false;
        yield return null;
    }

    public IEnumerator SetTrackingHeigh()
    {
        yield return new WaitForSeconds(3.0f);
        maincam.transform.parent.Translate(0, 3.0f, 0);
        yield return null;
    }

    public void FilterView()
    {
        if (!actioninProgress)
        {
            if (filteredview)
            {
                StartCoroutine(highlight(1.5f));
            }
            if (!filteredview)
            {
                StartCoroutine(normalcol(1.5f));
            }
            filteredview = !filteredview;
        }
        
    }

    public void SwitchCam()
    {
        if (maincam != null)
        {
            Debug.Log(maincam.cullingMask);
            maincam.cullingMask = switchview ? 4388 : 6181;
            switchview = !switchview;
        }
    }

	// Update is called once per frame
	void Update () {
        if (OVRInput.GetDown(OVRInput.Button.Three) || Input.GetKeyDown(KeyCode.A))
        {
            if (switchview)
            {
                AssetManager.DeactivateAsset();
            }
            SwitchCam();
        }
        if (OVRInput.GetDown(OVRInput.Button.Four) || Input.GetKeyDown(KeyCode.Z))
        {
            if(switchview)
            FilterView();
        }

          maincam.transform.parent.Rotate(Vector3.up,OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).x);

    }
}
