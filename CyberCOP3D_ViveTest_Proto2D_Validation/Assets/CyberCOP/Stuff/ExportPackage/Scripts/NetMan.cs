﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Text;
public class NetMan : MonoBehaviour
{
    static UdpClient serv;
    Thread thread;
    string message = "";
    string clientaddress = "";
    static UdpClient client;

    public NetworkActions NA;
    public Transform player;
    // Use this for initialization
    void Start()
    {
        NA = GetComponent<NetworkActions>();
    }
    private void OnEnable()
    {
        thread = new Thread(new ThreadStart(ThreadReceiver));
        thread.Start();
    }
    private void KnownClient(string address)
    {
        client = new UdpClient(address, 12345);
    }
    // Update is called once per frame
    void Update()
    {
        CheckMessage();
        if (Input.GetKeyDown(KeyCode.C))
        {
            DiscoverMsg();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Send("Test");
        }

    }

    public void DiscoverMsg()
    {
        UdpClient broad = new UdpClient();
        IPEndPoint ipbroad = new IPEndPoint(IPAddress.Broadcast, 12345);

        broad.EnableBroadcast = true;   
        byte[] msg = Encoding.ASCII.GetBytes("IP");
        broad.Send(msg, msg.Length, ipbroad);
        broad.EnableBroadcast = false;
        broad.Close();
    }

    public void Send(string s)
    {
        byte[] msg = Encoding.ASCII.GetBytes(s);
        client.Send(msg, msg.Length);
    }

    private void OnDisable()
    {
        if (client != null)
        {
            client.Close();
        }
        serv.Close();
        thread.Abort();
    }

    private void ThreadReceiver()
    {
        serv = new UdpClient(12345);
        while (true)
        {
            IPEndPoint Endpoint = new IPEndPoint(IPAddress.Any, 0);

            byte[] receive = serv.Receive(ref Endpoint);
            message = Endpoint.Address.ToString()+";"+Encoding.ASCII.GetString(receive);

        }

    }


    void CheckMessage()
    {
        if (message.Contains("IP"))
        {
            foreach(IPAddress ip in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (ip.Equals(IPAddress.Parse(message.Split(';')[0])))
                {
                    Debug.Log("c'est moi!");
                    message = "";
                    return;
                }
            }
            if (clientaddress == "")
            {
                clientaddress = message.Split(';')[0];
                Debug.Log("on a l'addresse du client et c'est ;" + clientaddress);
                KnownClient(clientaddress);
                Send("IP");
                message = "";
            }
            return;
        }

        if (message.Contains("Test"))
        {
            Debug.Log(message);            
            message = "";
            return;
        }

        if (message.Contains("Position"))
        {
            Send("Position;" + player.position.x + ";" + player.position.y +";"+ player.position.z);
            message = "";
            return;
        }

        if (message.Contains("M_AssetP"))
        {
            string[] s = message.Split(';');
            if (s[3] == "On")
            {
                Debug.Log("on map "+s[2]);
                NA.StartParticleMap(int.Parse(s[2]));
            }
            if (s[3] == "Off")
            {
                Debug.Log("off map " + s[2]);
                NA.StopParticleMap(int.Parse(s[2]));
            }
            message = "";
            return;
        }
        if (message.Contains("T_AssetP"))
        {
            string[] s = message.Split(';');
            if (s[3] == "On")
            {
                Debug.Log("T on " + s[2]);
                NA.StartParticleTopology(int.Parse(s[2]));
            }
            if (s[3] == "Off")
            {
                Debug.Log("T off " + s[2]);
                NA.StopParticleTopology(int.Parse(s[2]));
            }
            message = "";
            return;
        }
        if (message.Contains("Entropy_S"))
        {
            string[] s = message.Split(';');
            if (s[3] == "On")
            {
                Debug.Log("S on " + s[2]);
                NA.StartSound(int.Parse(s[2]));
            }
            if (s[3] == "Off")
            {
                Debug.Log("S off " + s[2]);
                //NA.StopParticleTopology(int.Parse(s[2]));
            }
            message = "";
            return;
        }
    }

}
