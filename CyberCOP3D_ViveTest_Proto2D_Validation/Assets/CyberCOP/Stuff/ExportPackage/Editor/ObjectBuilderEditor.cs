﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(AssetPositionsFilter))]
public class ObjectBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AssetPositionsFilter myScript = (AssetPositionsFilter)target;
        if (GUILayout.Button("TopologyLinks"))
        {
            myScript.CreateTopologyLinks();
        }
    }
}