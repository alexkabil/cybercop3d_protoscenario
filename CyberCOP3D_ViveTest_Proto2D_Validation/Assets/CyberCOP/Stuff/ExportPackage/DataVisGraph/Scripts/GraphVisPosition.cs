﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphVisPosition : MonoBehaviour {
    private int selectedAsset = -1;
    private int graphSelectedAsset = -1;
    private bool auto = false;
    private int activity = 0;
    private bool control = false;
    public AssetInfoList scenario;
    private int pattern = -1;
    // Use this for initialization
    void Start()
    {
        WriteData();
        displayAssets(false);
        
    }
    public int getSelectedAsset()
    {
        return selectedAsset;
    }
    public void displayAssets(bool b)
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(b);
        }
    }

    public void PositionAssets(int i)
    {
        PutTopologyLinks(false);
        //PutVersionLinks(false);
        displayAssets(true);
        if (selectedAsset != -1 && selectedAsset != i)
        {
            //transform.GetChild(selectedAsset).GetComponent<AssetVizControl>().isSelected = false;
        }
        selectedAsset = i;
        StartCoroutine(SmoothPositionning(i, 1.5f));
    }

    IEnumerator SmoothPositionning(int i, float t)
    {
        float timer = 0;
        while (timer < t)
        {
            timer += Time.deltaTime;
            for (int j = 0; j < transform.childCount; j++)
            {
                transform.GetChild(j).localPosition = Vector3.Lerp(transform.GetChild(j).localPosition, scenario.AssetsPositions[i].positions[j], Time.deltaTime * 1.5f);
            }
            yield return null;

        }
        for (int j = 0; j < transform.childCount; j++)
        {
            transform.GetChild(j).localPosition = scenario.AssetsPositions[i].positions[j];
        //    transform.GetChild(j).GetComponent<AssetVizControl>().SetColor(0);
        }
       // transform.GetChild(selectedAsset).GetComponent<AssetVizControl>().isSelected = true;
       // transform.GetChild(selectedAsset).GetComponent<AssetVizControl>().SetColor(1);
        yield return null;
    }

    public void PutTopologyLinks(bool b)
    {
        if (selectedAsset != -1)
        {
            for (int j = 0; j < transform.childCount; j++)
            {
                if (b)
                {
                    transform.GetChild(j).GetChild(0).GetComponent<LineRenderer>().enabled = true;
                    transform.GetChild(j).GetChild(0).GetComponent<LineRenderer>().SetPosition(1, scenario.AssetsPositions[selectedAsset].topology[j]);
                }
                else
                {
                    transform.GetChild(j).GetChild(0).GetComponent<LineRenderer>().enabled = false;
                }
            }
        }
    }

    public void PutVersionLinks(bool b)
    {
        if (selectedAsset != -1)
        {

            for (int j = 0; j < transform.childCount - 1; j++)
            {
                if (b)
                {
                    if (scenario.Assets[j].OS.Equals(scenario.Assets[selectedAsset].OS))
                    {
                        transform.GetChild(j).GetChild(1).GetComponent<LineRenderer>().enabled = true;
                        transform.GetChild(j).GetChild(1).GetComponent<LineRenderer>().SetPosition(1, transform.GetChild(selectedAsset).localPosition - transform.GetChild(j).localPosition);
                        //transform.GetChild(j).GetChild(1).GetComponent<ParticleSystem>().Play();
                    }
                }
                else
                {
                    //transform.GetChild(j).GetChild(1).GetComponent<ParticleSystem>().Stop();
                    transform.GetChild(j).GetChild(1).GetComponent<LineRenderer>().enabled = false;
                }
            }
        }
    }

    public void AutoToggle()
    {
        auto = !auto;
        if (auto)
        {
            if (activity == 0)
            {
                activity = 1;
                GetComponent<Animator>().SetTrigger("auto");
            }
        }
        else
        {
            if (activity > 0)
            {
                activity = 0;
                GetComponent<Animator>().SetTrigger("stop");
            }
        }
    }

    public void SetControl(float f)
    {
        if (control)
        {
            GetComponent<Animator>().SetFloat("Blend", f);
        }
    }

    public void ControlToggle()
    {
        control = !control;
        if (control)
        {
            if (activity == 0)
            {
                activity = 2;
                GetComponent<Animator>().SetTrigger("control");
            }
        }
        else
        {
            if (activity > 0)
            {
                activity = 0;
                GetComponent<Animator>().SetTrigger("stop");
            }
        }
    }


    public void SelectInGraph(int i)
    {
        if (i == selectedAsset)
        {
            return;
        }
        if (graphSelectedAsset != -1 && graphSelectedAsset != i)
        {
           // transform.GetChild(graphSelectedAsset).GetComponent<AssetVizControl>().SetColor(0);
        }
        graphSelectedAsset = i;
       // transform.GetChild(i).GetComponent<AssetVizControl>().SetColor(2);
    }

    public void SetSelectedAssetConfidenceLevel(float f)
    {

       // transform.GetChild(graphSelectedAsset).GetComponent<AssetVizControl>().SetConfidenceLevel(f);

    }
    public void IncrementGraph()
    {
        if (pattern>transform.childCount-1)
        {
            pattern--;
            return;
        }
        pattern++;
        PositionAssets(pattern);
    }
    public void DecrementGraph()
    {
        if (pattern < 0)
        {
            pattern = 0;
            return;
        }
        pattern--;
        PositionAssets(pattern);
    }

    public void WriteData()
    {
        for(int i = 0; i < scenario.Assets.Count; i++)
        {
            transform.GetChild(i).GetChild(2).GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = scenario.Assets[i].DataInfo();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            PositionAssets(pattern);
            pattern++;
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            pattern --;
            PositionAssets(pattern);   
        }
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            PutVersionLinks(true);
            WriteData();
        }
    }
}
