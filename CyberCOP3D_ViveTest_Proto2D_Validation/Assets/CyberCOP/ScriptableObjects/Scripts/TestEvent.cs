﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class TestEvent : ScriptableObject
{
    private List<TestEventListener> TEL = new List<TestEventListener>();
    public void Raise()
    {
        for(int i = TEL.Count - 1; i >= 0; i--)
        {
            TEL[i].OnEventRaised();
        }
    }
    public void RegisterListener(TestEventListener te)
    {
        if (!TEL.Contains(te))
        {
            TEL.Add(te);
        }
    }
    public void UnregisterListener(TestEventListener te)
    {
        if (TEL.Contains(te))
        {
            TEL.Remove(te);
        }
    }
}
