﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu]
public class FloatAssetList : ScriptableObject
{
    public List<FloatAsset> FloatList;
    public float meanValue;
    public void Additem(FloatAsset FA)
    {
        if (!FloatList.Contains(FA))
        {
            FloatList.Add(FA);
        }
    }
    public void RemoveItem(FloatAsset FA)
    {
        if (FloatList.Contains(FA))
        {
            FloatList.Remove(FA);
        }
    }

    public float MeanValue()
    {
        meanValue = FloatList.Select(x => x.Value).Average();
        return meanValue;
    }

}
