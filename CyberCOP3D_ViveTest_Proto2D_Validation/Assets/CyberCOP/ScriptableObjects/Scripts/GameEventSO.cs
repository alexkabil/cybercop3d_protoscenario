﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEventSO : ScriptableObject
{
    public float f_value = 0;
    public string s_value = "";

    private List<GameEventListener> listeners = new List<GameEventListener>();

    public void Raise(float f)
    {
        for(int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(f);
        }
    }
    public void Raise(string s)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(s);
        }
    }
    public void Raise(string s, int i, float f)
    {
        for (int j = listeners.Count - 1; j >= 0; j--)
        {
            listeners[j].OnEventRaised(s,i,f);
        }
    }
    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener list)
    {
        if (!listeners.Contains(list))
        {
            listeners.Add(list);
        }
    }

    public void UnregisterListener(GameEventListener list)
    {
        if (listeners.Contains(list))
        {
            listeners.Remove(list);
        }
    }

}
