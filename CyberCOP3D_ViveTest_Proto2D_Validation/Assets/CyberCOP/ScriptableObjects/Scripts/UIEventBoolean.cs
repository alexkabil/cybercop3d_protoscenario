﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class UIEventBoolean : ScriptableObject
{

    private List<UIEventListener> listeners = new List<UIEventListener>();

    public void Raise(bool b)
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(b);
        }
    }
    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(UIEventListener list)
    {
        if (!listeners.Contains(list))
        {
            listeners.Add(list);
        }
    }

    public void UnregisterListener(UIEventListener list)
    {
        if (listeners.Contains(list))
        {
            listeners.Remove(list);
        }
    }

}
