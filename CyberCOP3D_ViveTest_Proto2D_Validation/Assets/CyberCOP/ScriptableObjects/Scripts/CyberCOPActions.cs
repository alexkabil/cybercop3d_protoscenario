﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SystemActions
{
    //rajouter ici les ticket alertes et incidents ou les laisser à part comme des entités???
    BEGINATTACK,
    BEGINFALSEATTACK,
    NONE,
}

public enum AssetUpdateActions
{
    ADDMALICIOUSPROCESSES,
    ADDLEGITPROCESSES,
    METRICRAISE,
    ALERT,
    ASSETSELECTION,
    ASSETUNSELECTION,
    ASSETINFORMATION,
    ASSETANALYSIS,
    ASSETINVESTIGATION,
    ASSETCOMPROMISED,
    INCIDENT,
    NONE,
}

public enum TicketUpdateActions
{
    TICKETCREATION,
    TICKETSELECTION,//coord and analyst?
    TICKETUNSELECTION,
    TICKETPROGRESSION,
    TICKETVALIDATION,
    TICKETESCALATION,
    NONE,
}

public enum AlertUpdateActions
{
	ALERTCREATION,
	ALERTUPDATEINFORMATION,
	ALERTESCALATION,
	ALERTSELECTION,
	ALERTUNSELECTION,
	ALERTCHARACTERIZATION,
	NONE,
}

public enum IncidentUpdateActions
{
	INCIDENTCREATION,
	INCIDENTUPDATEINFORMATION,
	INCIDENTCORRELATION,
	INCIDENTSELECTION,
	INCIDENTUNSELECTION,
	INCIDENTVALIDATION,
	NONE,
}


public enum RequiredActions
{
    ALERTDETECTION,//toutes les actions meme systeme?
    ALERTSELECTION,
    ALERTUNSELECTION,
    ALERTSELECTION_COORD,
    ALERTSELECTION_ANALYST,
    ASK_ANALYST_CYBERINVESTIGATION,
    ASK_ANALYST_KINETICINVESTIGATION,
    ALERTINVESTIGATION,
    ALERTINVESTIGATION_COORD,
    ALERTINVESTIGATION_ANALYST,
    ASSETSELECTION,
    ASSETSELECTION_COORD,
    ASSETSELECTION_ANALYST,
    ASSETUNSELECTION,
    ASSETUNSELECTION_COORD,
    ASSETUNSELECTION_ANALYST,
    ASSETINFORMATION,
    ASSETINFORMATION_COORD,
    ASSETINFORMATION_ANALYST,
    ASSETINCIDENT,
    ASSETINCIDENT_COORD,
    ASSETINCIDENT_ANALYST,
    INCIDENTVALIDATION,
    INCIDENTVALIDATION_COORD,
    INCIDENTVALIDATION_ANALYST,
    NONE,
}