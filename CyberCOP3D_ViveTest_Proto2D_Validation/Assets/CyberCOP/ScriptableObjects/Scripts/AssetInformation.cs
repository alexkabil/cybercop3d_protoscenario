﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AssetInformation : ScriptableObject
{
    public AssetStatesInformation State;

    public string type;
    public string lastUser;
    public string lastIP;
    public string macAddress;
    public string OS;
    public string Version;
    public string criticity;
    public float cpuCharge;
    public FloatAsset networkCharge;
    public FloatAsset entropy;
    public List<string> processes;
    public string[] knownIssues;

    private string _type;
    private string _lastUser;
    private string _lastIP;
    private string _macAddress;
    private string _OS;
    private string _Version;
    private string _criticity;
    private float _cpuCharge;
    private List<string> _processes;
    private string[] _knownIssues;


    public void InitState()
    {
        _type = type;
        _lastUser = lastUser;
        _lastIP = lastIP;
        _macAddress = macAddress;
        _OS = OS;
        _Version = Version;
        _criticity = criticity;
        _cpuCharge = cpuCharge;
        _processes = new List<string>();
        foreach(string s in processes)
        {
            _processes.Add(s);
        }
        _knownIssues = knownIssues;
        networkCharge.InitState();
        entropy.InitState();
        State.InitState();

    }

    public void EndState()
    {
        type = _type;
        lastUser = _lastUser;
        lastIP = _lastIP;
        macAddress = _macAddress;
        OS = _OS;
        Version = _Version;
        criticity = _criticity;
        cpuCharge = _cpuCharge;
        processes.Clear();
        foreach(string s in _processes)
        {
            processes.Add(s);
        }
        processes = _processes;
        knownIssues = _knownIssues;
        networkCharge.EndState();
        entropy.EndState();
        State.EndState();
    }

    public string DataInfo()
    {
        string s = "Name : " + this.name + "\n Type : " + this.type + "\n Last User : " + this.lastUser + "\n Last IP : " + this.lastIP + "\n Mac Address : "
            + this.macAddress + "\n OS : " + this.OS + "\n Version : " + this.Version + "\n Criticity : " + this.criticity +
            "\n CPU Charge : " + this.cpuCharge+"\n Processes : ";
        foreach(string st in processes)
        {
            s += "\n\t" + st;
        }
        s += "\n Issues : ";
        foreach(string str in knownIssues)
        {
            s += "\n\t" + str;
        }
        return s;
    }

    public string AllPhysInfo()
    {
        string s = "Name : " + this.name + "\n Type : " + this.type + "\n Last User : " + this.lastUser + "\n OS : " + this.OS + "\n Version : " + this.Version + "\n Criticity : " + this.criticity +
            "\n CPU Charge : " + this.cpuCharge + "\n Processes : ";
        foreach (string st in processes)
        {
            s += "\n\t" + st;
        }
        return s;
    }

    public string PhysInfo()
    {
        string s = "Name : " + this.name + "\n Type : " + this.type + "\n Last User : " + this.lastUser + "\n OS : " + this.OS + 
            "\n Version : " + this.Version + "\n Criticity : " + "UNAUTHORIZED" +
            "\n CPU Charge : " + this.cpuCharge + "\n Processes : ";
        foreach (string st in processes)
        {
            s += "\n\t" + "UNAUTHORIZED";
        }
        return s;
    }

    public string InitPhysicInfo()
    {
        string s = "Name : " + this.name + "\n Type : " + "..." + "\n Last User : " + "..." + "..." + "\n OS : " +
            "..." + "\n Version : " + "..." + "\n Criticity : " + "..." +
            "\n CPU Charge : " + "..." + "\n Processes : ";
        foreach (string st in processes)
        {
            s += "\n\t" + "...";
        }

        return s;
    }

    public string AllCyberInfo()
    {
        string s = "Name : " + this.name + "\n Last IP : " + this.lastIP + "\n Mac Address : "
            + this.macAddress + "\n OS : " + this.OS + "\n Version : " + this.Version + "\n Criticity : " + this.criticity;
        s += "\n Issues : ";
        foreach (string str in knownIssues)
        {
            s += "\n\t" + str;
        }
        return s;
    }

    public string CyberInfo()
    {
        string s = "Name : " + this.name + "\n Last IP : " + this.lastIP + "\n Mac Address : "
             + this.macAddress + "\n OS : " + this.OS + "\n Criticity : " + "UNAUTHORIZED";
        s += "\n Issues : ";
        foreach (string str in knownIssues)
        {
            s += "\n\t" + "UNAUTHORIZED";
        }
        return s;
    }

    public string InitCyberInfo()
    {
        string s = "Name : " + this.name + "\n Last IP : " + "..." + "\n Mac Address : "
            + "..." + "\n OS : " + "..." + "\n Criticity : " + "...";
        s += "\n Issues : ";
        foreach (string str in knownIssues)
        {
            s += "\n\t" + "...";
        }

        return s;
    } 

}
