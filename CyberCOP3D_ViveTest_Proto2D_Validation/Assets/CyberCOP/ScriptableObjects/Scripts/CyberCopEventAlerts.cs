﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventAlerts : CyberCopEvent
{
    [SerializeField]
    private AlertUpdateActions alertAction = AlertUpdateActions.NONE;

    private AlertUpdateActions _alertAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _alertAction = alertAction;
        }
        if (!b)
        {
            alertAction = _alertAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)alertAction;
        base.Raise();
    }

}
