﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Roles
{
    ANALYST,
    COORDINATOR,
    OTHER,
}
[CreateAssetMenu]
public class UserStereotypeSO : ScriptableObject
{
    public RequiredActions[] PossibleActions;
    public Roles userRole;
    public string Name;
    public int id;
    public int CurrentTicketID;//mettre un index comme sur le scenar?
    public bool isImmersive;

    private int _currentTicketID;
    private bool _isImmersive;
    private Roles _userRole;

    public void InitState()
    {
        _currentTicketID = CurrentTicketID;
        _isImmersive = isImmersive;
        _userRole = userRole;
    }
    public void EndState()
    {
        CurrentTicketID = _currentTicketID;
        isImmersive = _isImmersive;
        userRole = _userRole;
    }
    //rajouter methodes d'initialisation?
}
