﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CyberCopEvent : ScriptableObject
{
    [SerializeField]
    private int asset = 0;
    [SerializeField]
    private int view = 0;

    protected int action = 0;

    [SerializeField]
    private int user = 0;
    [SerializeField]
    private int ticket = 0;

    private int _asset=0;
    private int _view = 0;
    private int _action = 0;
    private int _user = 0;
    private int _ticket = 0;

    //rajouter un event de log??
    private List<CyberCopEventsListener> listeners = new List<CyberCopEventsListener>();


    public virtual void InitValues(bool b)
    {
        if (b)
        {
            _asset = asset;
            _view = view;
            _action = action;
            _user = user;
            _ticket = ticket;
        }
        else
        {
            asset = _asset;
            view = _view;
            action = _action;
            user = _user;
            ticket = _ticket;
        }
    }

    public virtual void Raise()
    {
        CyberCopEventsInformation info = new CyberCopEventsInformation(asset, view, action, user, ticket);
        for (int k = listeners.Count - 1; k >= 0; k--)
        {
            listeners[k].OnEventRaised(info, false);//on passe false pour les tests ou pour les events systeme?
        }
    }

    public virtual void Raise(CyberCopEventsInformation i, bool b) //parametres et un bool de test? evenements generiques?
    {
        for (int k = listeners.Count - 1; k >= 0; k--)
        {
            listeners[k].OnEventRaised(i,b);
        }
    }


    public void RegisterListener(CyberCopEventsListener list)
    {
        InitValues(true);
        if (!listeners.Contains(list))
        {
            listeners.Add(list);
        }
    }

    public void UnregisterListener(CyberCopEventsListener list)
    {
        InitValues(false);
        if (listeners.Contains(list))
        {
            listeners.Remove(list);
        }
    }
}
