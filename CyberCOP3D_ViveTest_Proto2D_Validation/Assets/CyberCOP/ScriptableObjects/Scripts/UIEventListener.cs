﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class MyBoolEvent : UnityEvent<bool>
{

}


public class UIEventListener : MonoBehaviour
{
    public UIEventBoolean GE;
    public MyBoolEvent ResponseBool;

    // Use this for initialization
    void Start()
    {

    }

    private void OnEnable()
    {
        GE.RegisterListener(this);
    }
    private void OnDisable()
    {
        GE.UnregisterListener(this);
    }
    public void OnEventRaised(bool b)
    {
        ResponseBool.Invoke(b);
    }

    public void OnEventRaised()
    {
        ResponseBool.Invoke(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
