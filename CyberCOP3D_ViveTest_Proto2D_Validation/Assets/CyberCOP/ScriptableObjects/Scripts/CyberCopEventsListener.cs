﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CyberCopEventComposite: UnityEvent<CyberCopEventsInformation>
{

}
[System.Serializable]
public class CyberCopEventCompositeControl : UnityEvent<CyberCopEventsInformation, bool>
{

}

public class CyberCopEventsListener : MonoBehaviour
{
    public CyberCopEvent CyberCopEvent;
    public CyberCopEventCompositeControl CyberCOPEventResponse;

    private void OnEnable()
    {
        CyberCopEvent.RegisterListener(this);
    }
    private void OnDisable()
    {
        CyberCopEvent.UnregisterListener(this);
    }

    public void OnEventRaised(CyberCopEventsInformation i, bool b)
    {
        CyberCOPEventResponse.Invoke(i, b);
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}


}
