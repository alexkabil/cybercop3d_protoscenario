﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AssetPositionsFilter : ScriptableObject
{
    public Vector3[] positions;
    public Vector3[] topology;
    public Vector3[] version;
    public Vector3[] type;

    public void CreateTopologyLinks()
    {
        if (positions != null)
        {
            for (int i = 0; i < 5; i++)
            {
                if (topology != null)
                {
                    topology[i] = positions[12] - positions[i];
                }
            }
            for (int i = 5; i < 9; i++)
            {
                if (topology != null)
                {
                    topology[i] = positions[11] - positions[i];
                }
            }
            for (int i = 9; i < 13; i++)
            {
                if (topology != null)
                {
                    topology[i] = positions[13] - positions[i];
                    topology[13] = Vector3.zero;
                }
            }
        }
    }
}
