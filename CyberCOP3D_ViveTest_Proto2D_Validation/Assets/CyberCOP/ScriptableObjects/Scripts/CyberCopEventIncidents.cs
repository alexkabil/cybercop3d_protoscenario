﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventIncidents : CyberCopEvent
{
    [SerializeField]
    private IncidentUpdateActions incidentAction = IncidentUpdateActions.NONE;

    private IncidentUpdateActions _incidentAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _incidentAction = incidentAction;
        }
        if (!b)
        {
            incidentAction = _incidentAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)incidentAction;
        base.Raise();
    }

}
