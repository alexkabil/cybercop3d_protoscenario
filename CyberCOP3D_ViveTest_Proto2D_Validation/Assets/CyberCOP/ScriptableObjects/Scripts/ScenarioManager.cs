﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioManager : MonoBehaviour {
    public Transform GameObjectAssets;
    public AssetInfoList Scenario;
    public Roles UserRole;
    public UserStereotypeSO CurrentUser;

    // Use this for initialization
    private void OnEnable()
    {
        Scenario.InitState();
        Scenario.Currentuser = CurrentUser;
        /*foreach (UserStereotypeSO user in Scenario.Users)
        {
            if (user.userRole == UserRole)
            {
                Scenario.Currentuser = user;//marche pas avec 2 analystes ffs !
                break;
            }
        }
        */
    }
    private void OnDisable()
    {
        Scenario.EndState();
        StopAllCoroutines();

    }
    void Start ()
    {
        //StartCoroutine(StartSimpleScenario());
	}
    //prise en compte des différents events mais à changer
    public IEnumerator StartSimpleScenario()
    {
        bool test = false;
        yield return new WaitForSeconds(5.0f);
        Scenario.Attacks[0].BeginAttack(0);
        yield return new WaitForSeconds(1.0f);
        Scenario.Attacks[1].BeginAttack(1);
        yield return null;
        /*
        while (!test)
        {
            if (Scenario.incidentNumber == 1)
            {
                test = true;
                yield return new WaitForSeconds(5.0f);
                Scenario.Attacks[1].AttackEvent.Raise(3, 0, 1, 0);
                Alert.Raise(3, 0, 1, 0);
                yield return null;

            }
            yield return null;
        }
        */
        yield return new WaitForSeconds(30.0f);

        yield return null;
    }

    public void AssetSelectionState() { }

    public void CyberPhysicalView(int i)
    {
        if (i == 0)
        {
            for(int j=0; j<GameObjectAssets.childCount-1;j++)
            {
                GameObjectAssets.GetChild(j).GetChild(0).gameObject.SetActive(true);
                GameObjectAssets.GetChild(j).GetChild(1).gameObject.SetActive(false);   
            }
        }
        else if (i == 1)    
        {
            for (int j = 0; j < GameObjectAssets.childCount-1; j++)
            {
                GameObjectAssets.GetChild(j).GetChild(0).gameObject.SetActive(false);
                GameObjectAssets.GetChild(j).GetChild(1).gameObject.SetActive(true);
            }
        }
    }


	// Update is called once per frame
	void Update ()
    {
		
	}
}
