﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CyberCOPAttack : ScriptableObject
{
    public bool isFalsePositive;
    public int attackState=0;
    public  int cyberPhysical = 0;
    public  int assetConcerned = 0;
    public  int attackID=0;
    public RequiredActions[] RequiredActions;
    public TestUIEvents[] AttackEvent;
    private bool _isFalsePositive;
    private int _attackState;
    public List<RequiredActions> ActionsPerformed; 

	


    public void InitState()
    {
        _attackState = attackState;
        _isFalsePositive = isFalsePositive;
        ActionsPerformed = new List<RequiredActions>();
    }

    public void EndState()
    {
        attackState = _attackState;
        isFalsePositive = _isFalsePositive;
        ActionsPerformed.Clear();
    }

    public RequiredActions GetCurrentAction()
    {
        return RequiredActions[attackState];
    }
    /*On passe en parametre le numéro de l'alerte..*/
    public void BeginAttack(int i)
    {
        foreach (TestUIEvents Atk in AttackEvent)
        {
            Atk.Raise(assetConcerned, attackState, cyberPhysical, i);
        }
    }

    //dès qu'on progresse, on change aussi les actionsperformed

    public void NextScenarioAction()
    {
        if (attackState < RequiredActions.Length-1)
        {
            ActionsPerformed.Add(GetCurrentAction());
            attackState++;
            return;
        }
        if (attackState == RequiredActions.Length - 1)
        {
            Debug.Log("on a tout fait");
        }
    }

}
