﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CyberCopEventUsers : CyberCopEvent
{
    [SerializeField]
    private RequiredActions userAction = RequiredActions.NONE;

    private RequiredActions _userAction;

    public override void InitValues(bool b)
    {
        if (b)
        {
            _userAction = userAction;
        }
        if (!b)
        {
            userAction = _userAction;
        }
        base.InitValues(b);
    }

    public override void Raise()
    {
        action = (int)userAction;
        base.Raise();
    }

}
