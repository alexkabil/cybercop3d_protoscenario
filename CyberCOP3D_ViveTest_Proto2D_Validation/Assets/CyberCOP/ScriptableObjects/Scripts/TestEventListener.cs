﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestEventListener : MonoBehaviour {
    public TestEvent TE;
    public UnityEvent Response;
	// Use this for initialization
	void Start () {
		
	}
    private void OnEnable()
    {
        TE.RegisterListener(this);
    }
    private void OnDisable()
    {
        TE.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }
    // Update is called once per frame
    void Update () {
		
	}
}
