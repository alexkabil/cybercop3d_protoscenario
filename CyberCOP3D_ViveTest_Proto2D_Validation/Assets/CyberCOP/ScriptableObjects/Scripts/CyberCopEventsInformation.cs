﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberCopEventsInformation
{
    private int _asset;
    public int Asset { get { return _asset; } }//pas de set sur la variable, elle change pas?

    private int _view;
    public int View { get { return _view; } }

    private int _action;
    public int Action { get { return _action; } } //qui va dépendre de l'event..

    private int _user;
    public int User { get { return _user; } }

    private int _ticketID;
    public int TicketID { get { return _ticketID; } }

    public CyberCopEventsInformation(int asset, int view, int action, int user, int ticketid)
    {
        _asset = asset;
        _view = view;
        _action = action;
        _user = user;
        _ticketID = ticketid;
    }

}
